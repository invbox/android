package com.ecinv.db.datasource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.ecinv.activity.LoginActivity;
import com.ecinv.db.bo.Category;
import com.ecinv.db.bo.History;
import com.ecinv.db.bo.Item;
import com.ecinv.db.bo.User;
import com.ecinv.db.sqlitehelper.EcinvSQLiteHelper;
import com.ecinv.search.ResponseParser;
import com.ecinv.util.Listing;
import com.ecinv.util.Util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ItemDataSource {

	// Database fields
	private SQLiteDatabase database;
	private EcinvSQLiteHelper dbHelper;
	private CategoryDataSource categoryDS;

	public ItemDataSource(Context context) {
		dbHelper = new EcinvSQLiteHelper(context);
	}

	public void open() throws SQLException {
		if (database == null || !database.isOpen())
			database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		database.close();
	}

	public void addItem(Listing listing, String user) {

		// ContentValues values = new ContentValues();
		// values.put(EcinvSQLiteHelper.ITEMCOLUMN_USERID, user);
		// values.put(EcinvSQLiteHelper.ITEMCOLUMN_ITEMCODE, listing.getId());
		// values.put(EcinvSQLiteHelper.ITEMCOLUMN_DESCRIPTION,
		// listing.getTitle());
		// values.put(EcinvSQLiteHelper.ITEMCOLUMN_IMAGE,
		// listing.getImageUrl());
		// values.put(EcinvSQLiteHelper.ITEMCOLUMN_ITEMNAME,
		// listing.getTitle());
		// values.put(EcinvSQLiteHelper.ITEMCOLUMN_PURCHASEPRICE,
		// listing.getPurchasePrice());
		// values.put(EcinvSQLiteHelper.ITEMCOLUMN_TYPE, listing.getType());
		// values.put(EcinvSQLiteHelper.ITEMCOLUMN_STATUS, listing.getStatus());
		//
		// database.insert(EcinvSQLiteHelper.TABLE_ITEM, null, values);
		addItem(user, listing.getTitle(), listing.getImageUrl(),
				listing.getTitle(), listing.getPurchasePrice(),
				listing.getPurchaseDate(), listing.getId(), listing.getType(),
				listing.getStatus(), listing.getCategoryCode());

		return;
	}

	// Change to
	// public Item addItem(Item item)
	public Item addItem(String userid, String description, String image,
			String itemname, String purchasePrice, String purchaseDate,
			String itemCode, String type, String status, String categoryCode) {
		ContentValues values = new ContentValues();
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_USERID, userid);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_DESCRIPTION, description);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_IMAGE, image);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_ITEMNAME, itemname);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_PURCHASEPRICE, purchasePrice);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_PURCHASE_DATE, purchaseDate);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_ITEMCODE, itemCode);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_TYPE, type);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_STATUS, status + "-unsynched");
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_CATEGORY, "" + categoryCode);

		long insertId = database.insert(EcinvSQLiteHelper.TABLE_ITEM, null,
				values);

		if (insertId == -1)
			return null;

		// SimpleDateFormat purSDFDate = new SimpleDateFormat(
		// "yyyy-MM-dd hh:mm:ss");
		// Date purDate;
		// try {
		// purDate = purSDFDate.parse(purchaseDate.replace("T", " "));
		// } catch (ParseException e) {
		// // TODO Auto-generated catch block
		// // e.printStackTrace();
		// Log.w("dateformatexception", "purchaseDate::" + purchaseDate + "::"
		// + e.getMessage());
		// return null;
		// }
		String id = createInvboxItem(userid, description, image, itemname,
				purchasePrice, purchaseDate, itemCode, type, status,
				categoryCode);

		values = new ContentValues();
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_SERVERID, id);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_STATUS, status + "-synched");

		int rowsInserted = database.update(EcinvSQLiteHelper.TABLE_ITEM,
				values,
				EcinvSQLiteHelper.ITEMCOLUMN_ID + " = " + insertId + "", null);

		Log.w("rowInserted", "" + rowsInserted);
		if (rowsInserted > 0) {
			Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_ITEM,
					EcinvSQLiteHelper.allItemColumns,
					EcinvSQLiteHelper.ITEMCOLUMN_ID + " = " + insertId + "",
					null, null, null, null);
			cursor.moveToFirst();
			Item item = cursorToItem(cursor);
			cursor.close();
			return item;
		}
		return null;
	}

	private synchronized String createInvboxItem(String userid,
			String description, String image, String itemname,
			String purchasePrice, String purchaseDateStr, String itemCode,
			String type, String status, String categoryCode) {

		String result = null;
		String id = null;
		InputStream is = null;
		Calendar purchaseDate = null;

		Date purDate = Util.convertStringToDate(purchaseDateStr);
		if (purDate != null) {
			purchaseDate = Calendar.getInstance();
			purchaseDate.setTime(purDate);
		}

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("itemcode", itemCode));
		nameValuePairs.add(new BasicNameValuePair("itemname", itemname));
		nameValuePairs.add(new BasicNameValuePair("itemdescription",
				description));
		nameValuePairs.add(new BasicNameValuePair("purchaseprice",
				purchasePrice));
		if (purchaseDate != null) {
			nameValuePairs.add(new BasicNameValuePair("purchasedate",
					"date.struct"));
			nameValuePairs.add(new BasicNameValuePair("purchasedate_day", ""
					+ purchaseDate.get(Calendar.DAY_OF_MONTH)));
			nameValuePairs.add(new BasicNameValuePair("purchasedate_month", ""
					+ (purchaseDate.get(Calendar.MONTH) + 1)));
			nameValuePairs.add(new BasicNameValuePair("purchasedate_year", ""
					+ purchaseDate.get(Calendar.YEAR)));
			// need to add zone Z
		}

		nameValuePairs.add(new BasicNameValuePair("type", type));
		nameValuePairs.add(new BasicNameValuePair("image", image));
		nameValuePairs.add(new BasicNameValuePair("category.id", categoryCode));
		nameValuePairs.add(new BasicNameValuePair("create", "Create"));
		nameValuePairs.add(new BasicNameValuePair("profilecode", userid));
		nameValuePairs.add(new BasicNameValuePair("status", status));

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost("http://" + Util.SERVER_IP
					+ "/invbox/item/save?requesttype=xml");
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpPost.setHeader("Content-Type",
					"application/x-www-form-urlencoded");

			HttpResponse response = httpclient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			result = convertStreamToString(is);

			Document doc = new ResponseParser().xmlFromString(result);
			NodeList nodeList = doc.getElementsByTagName("item");

			Element itemObj = null;
			if (nodeList != null && nodeList.getLength() > 0)
				itemObj = (Element) nodeList.item(0);
			id = itemObj.getAttribute("id");

			Log.d("HTTP", "HTTP: OK::" + result);
		} catch (Exception e) {
			Log.e("HTTP", "Error in http connection " + e.toString());
		}
		return (id);
	}

	private static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append((line + "\n"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	public void updateItemStatus(Item item) {
		long id = item.getId();
		String serverid = item.getServerId();

		ContentValues values = new ContentValues();

		values = new ContentValues();
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_STATUS, item.getStatus()
				+ "-synched");

		long updateId = database.update(EcinvSQLiteHelper.TABLE_ITEM, values,
				EcinvSQLiteHelper.ITEMCOLUMN_ID + " = \"" + id + "\"", null);

		if (updateId == -1)
			return;
		updateId = updateInvboxItem(item.getUserid(), item.getDescription(),
				item.getImage(), item.getItemname(),
				"" + item.getPurchasePrice(), item.getPurchaseDate(),
				item.getItemCode(), item.getType(), item.getStatus(),
				item.getServerId(), "" + item.getCategoryCode());

		if (updateId == -1)
			return;
		// update to synched

		values = new ContentValues();
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_STATUS, item.getStatus()
				+ "-synched");
		updateId = database.update(EcinvSQLiteHelper.TABLE_ITEM, values,
				EcinvSQLiteHelper.ITEMCOLUMN_ID + " = \"" + id + "\"", null);
	}

	public void updateItem(Item item, String userid, String description,
			String image, String itemname, String purchasePrice,
			String purchaseDate, String itemCode, String type, String status,
			String categoryCode) {

		long id = item.getId();
		String serverid = item.getServerId();

		ContentValues values = new ContentValues();
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_USERID, userid);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_DESCRIPTION, description);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_IMAGE, image);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_ITEMNAME, itemname);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_PURCHASEPRICE, purchasePrice);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_PURCHASE_DATE, purchaseDate);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_ITEMCODE, itemCode);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_TYPE, type);
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_STATUS, status + "-unsynched");
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_CATEGORY, categoryCode);

		long updateId = database.update(EcinvSQLiteHelper.TABLE_ITEM, values,
				EcinvSQLiteHelper.ITEMCOLUMN_ID + " = \"" + id + "\"", null);

		if (updateId == -1)
			return;
		updateId = updateInvboxItem(userid, description, image, itemname,
				purchasePrice, purchaseDate, itemCode, type, status, serverid,
				categoryCode);

		if (updateId == -1)
			return;
		// update to synched

		values = new ContentValues();
		values.put(EcinvSQLiteHelper.ITEMCOLUMN_STATUS, status + "-synched");
		updateId = database.update(EcinvSQLiteHelper.TABLE_ITEM, values,
				EcinvSQLiteHelper.ITEMCOLUMN_ID + " = \"" + id + "\"", null);
	}

	public synchronized int updateInvboxItemStatus(Item item) {

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("id", item.getServerId()));
		String tempStore;
		if ((tempStore = item.getItemCode()) != null && !"".equals(tempStore)) {
			nameValuePairs.add(new BasicNameValuePair("itemcode", tempStore));
		}
		if ((tempStore = item.getItemname()) != null && !"".equals(tempStore)) {
			nameValuePairs.add(new BasicNameValuePair("itemname", tempStore));
		}
		if ((tempStore = item.getDescription()) != null
				&& !"".equals(tempStore)) {
			nameValuePairs.add(new BasicNameValuePair("itemdescription",
					tempStore));
		}
		if ((tempStore = "" + item.getPurchasePrice()) != null
				&& !"".equals(tempStore)) {
			nameValuePairs.add(new BasicNameValuePair("purchaseprice",
					tempStore));
		}
		Date purDate = Util.convertStringToDate(item.getPurchaseDate());
		Calendar purchaseDate = null;
		if (purDate != null) {
			purchaseDate = Calendar.getInstance();
			purchaseDate.setTime(purDate);
		}

		if (purchaseDate != null) {
			nameValuePairs.add(new BasicNameValuePair("purchasedate",
					"date.struct"));
			nameValuePairs.add(new BasicNameValuePair("purchasedate_day", ""
					+ purchaseDate.get(Calendar.DAY_OF_MONTH)));
			nameValuePairs.add(new BasicNameValuePair("purchasedate_month", ""
					+ (purchaseDate.get(Calendar.MONTH) + 1)));
			nameValuePairs.add(new BasicNameValuePair("purchasedate_year", ""
					+ purchaseDate.get(Calendar.YEAR)));
			// need to add zone Z
		}
		if ((tempStore = item.getType()) != null && !"".equals(tempStore)) {
			nameValuePairs.add(new BasicNameValuePair("type", tempStore));
		}
		if ((tempStore = item.getImage()) != null && !"".equals(tempStore)) {
			nameValuePairs.add(new BasicNameValuePair("image", tempStore));
		}
		if ((tempStore = "" + item.getCategoryCode()) != null
				&& !"".equals(tempStore)) {
			nameValuePairs
					.add(new BasicNameValuePair("category.id", tempStore));
		}
		// nameValuePairs.add(new BasicNameValuePair("_action_update",
		// "Update"));
		if ((tempStore = item.getUserid()) != null && !"".equals(tempStore)) {
			nameValuePairs
					.add(new BasicNameValuePair("profilecode", tempStore));
		}
		if ((tempStore = item.getStatus()) != null && !"".equals(tempStore)) {
			nameValuePairs.add(new BasicNameValuePair("status", tempStore));
		}

		serverCall(nameValuePairs);
		return 0;
	}

	private synchronized int updateInvboxItem(String userid,
			String description, String image, String itemname,
			String purchasePrice, String purchaseDateStr, String itemCode,
			String type, String status, String serverid, String categoryCode) {

		String result = null;
		InputStream is = null;
		Calendar purchaseDate = null;

		Date purDate = Util.convertStringToDate(purchaseDateStr);
		if (purDate != null) {
			purchaseDate = Calendar.getInstance();
			purchaseDate.setTime(purDate);
		}

		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair("id", serverid));
		nameValuePairs.add(new BasicNameValuePair("itemcode", itemCode));
		nameValuePairs.add(new BasicNameValuePair("itemname", itemname));
		nameValuePairs.add(new BasicNameValuePair("itemdescription",
				description));
		nameValuePairs.add(new BasicNameValuePair("purchaseprice",
				purchasePrice));
		if (purchaseDate != null) {
			nameValuePairs.add(new BasicNameValuePair("purchasedate",
					"date.struct"));
			nameValuePairs.add(new BasicNameValuePair("purchasedate_day", ""
					+ purchaseDate.get(Calendar.DAY_OF_MONTH)));
			nameValuePairs.add(new BasicNameValuePair("purchasedate_month", ""
					+ (purchaseDate.get(Calendar.MONTH) + 1)));
			nameValuePairs.add(new BasicNameValuePair("purchasedate_year", ""
					+ purchaseDate.get(Calendar.YEAR)));
			// need to add zone Z
		}
		nameValuePairs.add(new BasicNameValuePair("type", type));
		nameValuePairs.add(new BasicNameValuePair("image", image));
		nameValuePairs.add(new BasicNameValuePair("category.id", categoryCode));
		// nameValuePairs.add(new BasicNameValuePair("_action_update",
		// "Update"));
		nameValuePairs.add(new BasicNameValuePair("profilecode", userid));
		nameValuePairs.add(new BasicNameValuePair("status", status));

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost("http://" + Util.SERVER_IP
					+ "/invbox/item/update");
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpPost.setHeader("Content-Type",
					"application/x-www-form-urlencoded");
			HttpResponse response = httpclient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			result = convertStreamToString(is);

			Document doc = new ResponseParser().xmlFromString(result);
			NodeList nodeList = doc.getElementsByTagName("item");

			Element itemObj = null;
			if (nodeList != null && nodeList.getLength() > 0)
				itemObj = (Element) nodeList.item(0);
			String id = itemObj.getAttribute("id");

			if (id != null && !"".equals(id)) {
				Log.d("HTTP", "HTTP: OK::" + result);
				return 0;
			}
		} catch (Exception e) {
			Log.e("HTTP", "Error in http connection " + e.toString());
			// return -1;
		}
		return -1;
	}

	private int serverCall(ArrayList<NameValuePair> nameValuePairs) {
		String result = null;
		InputStream is = null;

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost("http://" + Util.SERVER_IP
					+ "/invbox/item/update");
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpPost.setHeader("Content-Type",
					"application/x-www-form-urlencoded");
			HttpResponse response = httpclient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			result = convertStreamToString(is);

			Document doc = new ResponseParser().xmlFromString(result);
			NodeList nodeList = doc.getElementsByTagName("item");

			Element itemObj = null;
			if (nodeList != null && nodeList.getLength() > 0)
				itemObj = (Element) nodeList.item(0);
			String id = itemObj.getAttribute("id");

			if (id != null && !"".equals(id)) {
				Log.d("HTTP", "HTTP: OK::" + result);
				return 0;
			}
		} catch (Exception e) {
			Log.e("HTTP", "Error in http connection " + e.toString());
			// return -1;
		}
		return -1;
	}

	public void deleteItem(Item item) {
		long id = item.getId();
		database.delete(EcinvSQLiteHelper.TABLE_ITEM,
				EcinvSQLiteHelper.ITEMCOLUMN_ID + " = \"" + id + "\"", null);
		updateInvboxItem(item.getUserid(), item.getDescription(),
				item.getImage(), item.getItemname(),
				"" + item.getPurchasePrice(), item.getPurchaseDate(),
				item.getItemCode(), item.getType(), "deleted",
				item.getServerId(), "" + item.getCategoryCode());
	}

	public void deleteItems(ArrayList<Item> list) {
		for (int count = 0; count < list.size(); count++) {
			Item item = list.get(count);
			deleteItem(item);
		}
	}

	public void updateItems(ArrayList<Item> list, ContentValues values) {
		for (int count = 0; count < list.size(); count++) {
			Item item = list.get(count);
			long id = item.getId();

			database.update(EcinvSQLiteHelper.TABLE_ITEM, values,
					EcinvSQLiteHelper.ITEMCOLUMN_ID + " = \"" + id + "\"", null);

			updateInvboxItem(item.getUserid(), item.getDescription(),
					item.getImage(), item.getItemname(),
					"" + item.getPurchasePrice(), item.getPurchaseDate(),
					item.getItemCode(), item.getType(),
					(String) values.get("status"), item.getServerId(), ""
							+ item.getCategoryCode());
		}
	}

	public List<Item> getItems(CategoryDataSource categoryDS) {
		List<Item> item = new ArrayList<Item>();

		Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_ITEM,
				EcinvSQLiteHelper.allItemColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Item itemObj = cursorToItem(cursor);// , categoryDS);
			item.add(itemObj);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return item;
	}
	
	public Item getItem(String id) {
//		List<Item> item = new ArrayList<Item>();
		
		Item item = null;

		Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_ITEM,
				EcinvSQLiteHelper.allItemColumns, "id="+id, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			item = cursorToItem(cursor);// , categoryDS);
//			item.add(itemObj);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return item;
	}
	

	public List<Item> getItemsForTypesAndStatuses(String[] types,
			String[] statuses, CategoryDataSource categoryDS) {
		List<Item> item = new ArrayList<Item>();

		StringBuffer statusStr = new StringBuffer();
		for (int count = 0; (types != null && count < types.length); count++) {
			statusStr.append(" type='" + types[count] + "' ");
			if ((count + 1) < types.length) {
				statusStr.append(" or ");
			}
		}
		if (statuses != null && statuses.length > 0) {
			statusStr.append(" and ");
		}
		for (int count = 0; (statuses != null && count < statuses.length); count++) {
			statusStr.append(" status='" + statuses[count] + "' ");
			if ((count + 1) < statuses.length) {
				statusStr.append(" or ");
			}
		}

		String userStr = " and profileCode='" + LoginActivity.getLoggedInUser()
				+ "' ";	

		Log.w("where", "" + statusStr.toString() + userStr);

		Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_ITEM,
				EcinvSQLiteHelper.allItemColumns, statusStr.toString()
						+ userStr, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Item itemObj = cursorToItem(cursor);// , categoryDS);
			item.add(itemObj);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return item;
	}

	private Item cursorToItem(Cursor cursor, CategoryDataSource categoryDS) {
		Item item = new Item();
		Category category = null;

		if (cursor.getCount() <= 0)
			return null;

		item.setId(cursor.getLong(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_ID)));
		item.setUserid(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_USERID)));
		item.setDescription(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_DESCRIPTION)));
		item.setImage(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_IMAGE)));
		item.setItemname(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_ITEMNAME)));
		item.setItemCode(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_ITEMCODE)));
		item.setPurchasePrice(cursor.getDouble(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_PURCHASEPRICE)));
		item.setPurchaseDate(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_PURCHASE_DATE)));
		item.setType(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_TYPE)));
		item.setStatus(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_STATUS)));
		item.setServerId(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_SERVERID)));
		item.setCategoryCode(cursor.getInt(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_CATEGORY)));

		cursor.close();

		return item;
	}

	private Item cursorToItem(Cursor cursor) {
		Item item = new Item();

		if (cursor.getCount() <= 0)
			return null;

		item.setId(cursor.getLong(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_ID)));
		item.setUserid(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_USERID)));
		item.setDescription(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_DESCRIPTION)));
		item.setImage(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_IMAGE)));
		item.setItemname(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_ITEMNAME)));
		item.setItemCode(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_ITEMCODE)));
		item.setPurchasePrice(cursor.getDouble(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_PURCHASEPRICE)));
		item.setPurchaseDate(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_PURCHASE_DATE)));
		item.setType(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_TYPE)));
		item.setStatus(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_STATUS)));
		item.setServerId(cursor.getString(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_SERVERID)));
		item.setCategoryCode(cursor.getInt(cursor
				.getColumnIndex(EcinvSQLiteHelper.ITEMCOLUMN_CATEGORY)));

		return item;
	}
}
