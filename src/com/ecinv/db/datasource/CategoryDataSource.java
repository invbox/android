package com.ecinv.db.datasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ecinv.db.bo.Category;
import com.ecinv.db.bo.History;
import com.ecinv.db.bo.User;
import com.ecinv.db.sqlitehelper.EcinvSQLiteHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class CategoryDataSource {

	// Database fields
	private SQLiteDatabase database;
	private EcinvSQLiteHelper dbHelper;

	public CategoryDataSource(Context context) {
		dbHelper = new EcinvSQLiteHelper(context);
	}

	public void open() throws SQLException {
		if (database == null || !database.isOpen())
			database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		database.close();
	}

	public List<Category> getCategories() {
		List<Category> category = new ArrayList<Category>();

		Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_CATEGORY,
				EcinvSQLiteHelper.allCategoryColumns, null, null, null, null,
				null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Category categoryObj = cursorToCategory(cursor);
			category.add(categoryObj);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return category;
	}
	
	public Category getCategoryObject(String categoryCode) {

		Category categoryObj = null;
		Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_CATEGORY,
				EcinvSQLiteHelper.allCategoryColumns, EcinvSQLiteHelper.CATEGORY_CATEGORYCODE+" = \""+categoryCode+"\"", null, null, null,
				null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			categoryObj = cursorToCategory(cursor);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return categoryObj;
	}

	private Category cursorToCategory(Cursor cursor) {
		Category category = new Category();

		if (cursor.getCount() <= 0)
			return null;

		category.setCategorycode(cursor.getString(0));
		category.setCategoryname(cursor.getString(1));
		category.setCategorydescription(cursor.getString(2));
		return category;
	}
}
