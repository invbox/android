package com.ecinv.db.datasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ecinv.db.bo.History;
import com.ecinv.db.bo.Item;
import com.ecinv.db.bo.Preferences;
import com.ecinv.db.bo.User;
import com.ecinv.db.sqlitehelper.EcinvSQLiteHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class UserPreferencesDataSource {

	// Database fields
	private SQLiteDatabase database;
	private EcinvSQLiteHelper dbHelper;

	public UserPreferencesDataSource(Context context) {
		dbHelper = new EcinvSQLiteHelper(context);
	}

	public void open() throws SQLException {
		if (database == null || !database.isOpen())
			database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		database.close();
	}

	/*
	 * public Preferences addToPreferences(String userid, String itemCode) {
	 * ContentValues values = new ContentValues();
	 * values.put(EcinvSQLiteHelper.HISTORYCOLUMN_USERID, userid);
	 * values.put(EcinvSQLiteHelper.HISTORYCOLUMN_ITEM_CODE, itemCode);
	 * values.put(EcinvSQLiteHelper.HISTORYCOLUMN_SEARCHDATE, new
	 * Date().toString());
	 * 
	 * long insertId = database.insert(EcinvSQLiteHelper.TABLE_HISTORY, null,
	 * values);
	 * 
	 * if (insertId == -1) return null;
	 * 
	 * Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_HISTORY,
	 * EcinvSQLiteHelper.allHistoryColumns, EcinvSQLiteHelper.HISTORYCOLUMN_ID +
	 * " = \"" + insertId + "\"", null, null, null, null); cursor.moveToFirst();
	 * History history = cursorToHistory(cursor); cursor.close(); return
	 * history; }
	 */
//	public void deleteHistory(Preferences preferences) {
//		long id = preferences.getId();
//		System.out.println("User deleted with id: " + id);
//		database.delete(EcinvSQLiteHelper.TABLE_HISTORY,
//				EcinvSQLiteHelper.HISTORYCOLUMN_ID + " = \"" + id + "\"", null);
//	}
//	
//	public void deleteItems(ArrayList<Preferences> list) {
//		for (int count = 0; count < list.size(); count++) {
//			Preferences item = list.get(count);
//			deleteItem(item);
//		}
//	}

	public List<Preferences> getPreferencesForUser(String userid) {
		List<Preferences> preferences = new ArrayList<Preferences>();

		String query = "select * from marketplace m left join userpreferences u on m.marketplacecode = u.marketplacecode and u.profilecode = ? order by marketplacename";
		String[] selectionArgs = { userid };

		Cursor cursor = database.rawQuery(query, selectionArgs);

		cursor.moveToFirst();
		Preferences prefObj;
		while (!cursor.isAfterLast()) {
			prefObj = cursorToPreferences(cursor);
			preferences.add(prefObj);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return preferences;
	}

	private Preferences cursorToPreferences(Cursor cursor) {
		Preferences preferences = new Preferences();

		if (cursor.getCount() <= 0)
			return null;

		 preferences.setMarketplacename(cursor.getString(1));
		 preferences.setMarketplacecode(cursor.getString(0));
		return preferences;
	}
}
