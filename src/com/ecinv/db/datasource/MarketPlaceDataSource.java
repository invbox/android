package com.ecinv.db.datasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ecinv.db.bo.History;
import com.ecinv.db.bo.User;
import com.ecinv.db.sqlitehelper.EcinvSQLiteHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class MarketPlaceDataSource {

	// Database fields
	private SQLiteDatabase database;
	private EcinvSQLiteHelper dbHelper;

	public MarketPlaceDataSource(Context context) {
		dbHelper = new EcinvSQLiteHelper(context);
	}

	public void open() throws SQLException {
		if (database == null || !database.isOpen())
			database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		database.close();
	}

	/*public Preferences addToPreferences(String userid, String itemCode) {
		ContentValues values = new ContentValues();
		values.put(EcinvSQLiteHelper.HISTORYCOLUMN_USERID, userid);
		values.put(EcinvSQLiteHelper.HISTORYCOLUMN_ITEM_CODE, itemCode);
		values.put(EcinvSQLiteHelper.HISTORYCOLUMN_SEARCHDATE, new Date().toString());

		long insertId = database.insert(EcinvSQLiteHelper.TABLE_HISTORY, null,
				values);

		if (insertId == -1)
			return null;

		Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_HISTORY,
				EcinvSQLiteHelper.allHistoryColumns,
				EcinvSQLiteHelper.HISTORYCOLUMN_ID + " = \"" + insertId + "\"",
				null, null, null, null);
		cursor.moveToFirst();
		History history = cursorToHistory(cursor);
		cursor.close();
		return history;
	}
*/
	public void deleteHistory(History history) {
		long id = history.getId();
		System.out.println("User deleted with id: " + id);
		database.delete(EcinvSQLiteHelper.TABLE_HISTORY,
				EcinvSQLiteHelper.HISTORYCOLUMN_ID + " = \"" + id + "\"", null);
	}

	public List<History> getPreferences() {
		List<History> history = new ArrayList<History>();

		Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_MARKETPLACE,
				EcinvSQLiteHelper.allMarketPlaceColumns, null, null, null, null,
				null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			History historyObj = cursorToHistory(cursor);
			history.add(historyObj);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return history;
	}

	private History cursorToHistory(Cursor cursor) {
		History history = new History();

		if (cursor.getCount() <= 0)
			return null;

		history.setUserId(cursor.getString(0));
		history.setContent(cursor.getString(1));
		return history;
	}
}
