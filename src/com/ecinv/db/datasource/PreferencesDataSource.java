package com.ecinv.db.datasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ecinv.activity.LoginActivity;
import com.ecinv.db.bo.History;
import com.ecinv.db.bo.Item;
import com.ecinv.db.bo.Preferences;
import com.ecinv.db.bo.User;
import com.ecinv.db.sqlitehelper.EcinvSQLiteHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class PreferencesDataSource {

	// Database fields
	private SQLiteDatabase database;
	private EcinvSQLiteHelper dbHelper;

	public PreferencesDataSource(Context context) {
		dbHelper = new EcinvSQLiteHelper(context);
	}

	public void open() throws SQLException {
		if (database == null || !database.isOpen())
			database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		database.close();
	}

	public void deleteUserPreference(String id) {
		// String id = preferences.getPreferenceCode();
		System.out.println("User deleted with id: " + id);
		database.delete(EcinvSQLiteHelper.TABLE_USERPREFERENCES,
				EcinvSQLiteHelper.USERPREFERENCES_PREFERENCECODE + " = \"" + id
						+ "\"", null);
	}

	//
	// public void deleteItems(ArrayList<Preferences> list) {
	// for (int count = 0; count < list.size(); count++) {
	// Preferences item = list.get(count);
	// deleteItem(item);
	// }
	// }

	public Preferences addUserPreference(String mktPlaceCode) {
		ContentValues values = new ContentValues();
		values.put(EcinvSQLiteHelper.USERPREFERENCES_USERID,
				LoginActivity.userid);
		values.put(EcinvSQLiteHelper.USERPREFERENCES_MARKETPLACECODE,
				mktPlaceCode);

		long insertId = database.insert(
				EcinvSQLiteHelper.TABLE_USERPREFERENCES, null, values);

		if (insertId == -1)
			return null;

		String query = "select m.*,u.marketPlaceCode as userMktCode, u.preferenceCode as userPrefCode from marketplace m left join userpreferences u on m.marketplacecode = u.marketplacecode and u.profilecode = ? and u.preferenceCode = ? order by marketplacename";
		String[] selectionArgs = { LoginActivity.userid, "" + insertId };

		Cursor cursor = database.rawQuery(query, selectionArgs);

		cursor.moveToFirst();
		Preferences preferences = cursorToPreferences(cursor);
		cursor.close();

		return preferences;
	}

	public void updateUserPreference(String mktPlaceCode, String preferenceCode) {

		deleteUserPreference(preferenceCode);
	}

	public List<Preferences> getPreferencesForUser(String userid) {
		List<Preferences> preferences = new ArrayList<Preferences>();

		String query = "select m.*,u.marketPlaceCode as userMktCode, u.preferenceCode as userPrefCode from marketplace m left join userpreferences u on m.marketplacecode = u.marketplacecode and u.profilecode = ? order by marketplacename";
		String[] selectionArgs = { userid };

		Cursor cursor = database.rawQuery(query, selectionArgs);

		cursor.moveToFirst();
		Preferences prefObj = null;
		while (!cursor.isAfterLast()) {
			prefObj = cursorToPreferences(cursor);
			preferences.add(prefObj);
			String uMktcode = cursor.getString(cursor
					.getColumnIndex("userMktCode"));
			if ((uMktcode != null) && (!"".equals(uMktcode))) {
				prefObj.setChecked(true);
				prefObj.setPrevState(true);
			} else {
				prefObj.setChecked(false);
				prefObj.setPrevState(false);
			}
			prefObj.setPreferenceCode(uMktcode);

			cursor.moveToNext();
		}

		// Make sure to close the cursor
		cursor.close();
		return preferences;
	}

	private Preferences cursorToPreferences(Cursor cursor) {
		Preferences preferences = new Preferences();

		if (cursor.getCount() <= 0)
			return null;

		preferences.setMarketplacecode(cursor.getString(0));
		preferences.setMarketplacename(cursor.getString(1));
		preferences.setMarketplacedescription(cursor.getString(2));
		preferences.setWebsite(cursor.getString(3));
		preferences.setEmail(cursor.getString(4));

		return preferences;
	}
}
