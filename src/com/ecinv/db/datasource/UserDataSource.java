package com.ecinv.db.datasource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ecinv.db.bo.User;
import com.ecinv.db.sqlitehelper.EcinvSQLiteHelper;
import com.ecinv.util.Base64;
import com.ecinv.util.CryptographyUtil;
import com.ecinv.util.Util;

public class UserDataSource {

	// Database fields
	private SQLiteDatabase database;
	private EcinvSQLiteHelper dbHelper;

	public UserDataSource(Context context) {
		dbHelper = new EcinvSQLiteHelper(context);
	}

	public void open() throws SQLException {
		if (database == null || !database.isOpen())
			database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public User createUser(String userid, String firstname, String lastname,
			String address, String password) {
		ContentValues values = new ContentValues();
		values.put(EcinvSQLiteHelper.USERCOLUMN_USERID, userid.trim());
		values.put(EcinvSQLiteHelper.USERCOLUMN_FIRSTNAME, firstname.trim());
		values.put(EcinvSQLiteHelper.USERCOLUMN_LASTNAME, lastname.trim());
		values.put(EcinvSQLiteHelper.USERCOLUMN_ADDRESS, address);
		values.put(EcinvSQLiteHelper.USERCOLUMN_PASSWORD, password);

		long insertId = database.insert(EcinvSQLiteHelper.TABLE_USER, null,
				values);

		if (insertId == -1)
			return null;

		createInvboxUser(userid, firstname, lastname, address, password);

		Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_USER,
				EcinvSQLiteHelper.allUserColumns,
				EcinvSQLiteHelper.USERCOLUMN_USERID + " = \"" + userid + "\"",
				null, null, null, null);
		cursor.moveToFirst();
		User newUser = cursorToUser(cursor);
		cursor.close();
		return newUser;
	}

	public boolean verifyUserCredentials(String userid, String password) {
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		String result;
		boolean verified = false;

		try {
			nameValuePairs.add(new BasicNameValuePair("password",Base64.encodeBytes(password.getBytes())));
			nameValuePairs.add(new BasicNameValuePair("profileCode", userid));
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost("http://" + Util.SERVER_IP
					+ "/invbox/userprofile/verify");
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpPost.setHeader("Content-Type",
					"application/x-www-form-urlencoded");
			HttpResponse response = httpclient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			result = convertStreamToString(entity.getContent());
			if (result != null && "true".equalsIgnoreCase(result.trim())) {
				verified = true;
			}
		} catch (Exception e) {
			Log.e("HTTP", "Error in http connection " + e.toString());
			verified = false;
		}

		return verified;
	}

	private static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append((line + "\n"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	private String createInvboxUser(String userid, String firstname,
			String lastname, String address, String password) {

		// Request paramters for a post call to create an user in the server
		// profileCode=o%40o.com&profileName=o&firstname=o&lastname=o&email=o%40o.com&address=&retainHistory=&password=1234&create=Create

		String result = null;
		InputStream is = null;
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("firstname", firstname));
		nameValuePairs.add(new BasicNameValuePair("lastname", lastname));
		nameValuePairs.add(new BasicNameValuePair("email", userid));
		nameValuePairs.add(new BasicNameValuePair("address", address));
		nameValuePairs.add(new BasicNameValuePair("retainHistory", "1"));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		nameValuePairs.add(new BasicNameValuePair("profileCode", userid));
		nameValuePairs.add(new BasicNameValuePair("create", "Create"));

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost("http://" + Util.SERVER_IP
					+ "/invbox/userprofile/save");
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpPost.setHeader("Content-Type",
					"application/x-www-form-urlencoded");
			HttpResponse response = httpclient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			byte[] b = new byte[1024];
			while (is.available() > 0) {
				is.read(b);
				Log.w("saveerror", new String(b));
			}

			Log.d("HTTP", "HTTP: OK");
		} catch (Exception e) {
			Log.e("HTTP", "Error in http connection " + e.toString());
		}
		return (result);
	}

	public void deleteUser(User user) {
		String id = user.getProfileCode();
		System.out.println("User deleted with id: " + id);
		database.delete(EcinvSQLiteHelper.TABLE_USER,
				EcinvSQLiteHelper.USERCOLUMN_USERID + " = \"" + id + "\"", null);
	}

	public List<User> getAllUsers() {
		List<User> users = new ArrayList<User>();

		Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_USER,
				EcinvSQLiteHelper.allUserColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			User user = cursorToUser(cursor);
			users.add(user);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return users;
	}

	public String getUserPwdFromLocal(String userid) {

		Cursor cursor = database.query(EcinvSQLiteHelper.TABLE_USER,
				EcinvSQLiteHelper.allUserColumns,
				EcinvSQLiteHelper.USERCOLUMN_USERID + " = \"" + userid + "\"",
				null, null, null, null);

		cursor.moveToFirst();
		User user = cursorToUser(cursor);

		// Make sure to close the cursor
		cursor.close();
		if (user == null)
			return null;
		return user.getPassword();
	}

	private User cursorToUser(Cursor cursor) {
		User user = new User();

		if (cursor.getCount() <= 0)
			return null;

		user.setId(cursor.getString(0));
		user.setfirstName(cursor.getString(1));
		user.setlastName(cursor.getString(2));
		user.setAddress(cursor.getString(3));
		user.setPassword(cursor.getString(7));
		return user;
	}
}
