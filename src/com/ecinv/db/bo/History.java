package com.ecinv.db.bo;

public class History {
	private String userid;
	private String content;
	private long id;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getUserId() {
		return userid;
	}

	public void setUserId(String id) {
		this.userid = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	// Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
		return userid + " " + content;
	}

}