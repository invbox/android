package com.ecinv.db.bo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Item {

	private String userid;
	private String itemCode;
	private String description;
	private String image; // blob
	private String itemname;
	private long id;
	private boolean checked;
	private double purchasePrice;
	private String purchaseDate;
	private String type;
	// inventory(-unsynched,-synched)/market/sold/
	private String status;
	private String serverId;
	private int categoryCode;

	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public double getPurchasePrice() {
		return this.purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public String getUserid() {
		return userid;
	}

	public void toggleChecked() {
		checked = !checked;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
		return userid;
	}

	// Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
		return this.userid + "-" + this.itemname;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getPurchaseDate() {
		return purchaseDate;
	}

	public void setPurchaseDate(String purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	public int getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(int categoryCode) {
		this.categoryCode = categoryCode;
	}

}