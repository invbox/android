package com.ecinv.db.bo;

public class UserPreferences {
	private String marketplacecode;
	private String preferenceCode;
	private String profileCode;

	


	public String getMarketplacecode() {
		return marketplacecode;
	}

	public void setMarketplacecode(String marketplacecode) {
		this.marketplacecode = marketplacecode;
	}

	public String getPreferenceCode() {
		return preferenceCode;
	}


	public void setPreferenceCode(String preferenceCode) {
		this.preferenceCode = preferenceCode;
	}

	public String getProfileCode() {
		return profileCode;
	}

	public void setProfileCode(String profileCode) {
		this.profileCode = profileCode;
	}

	// Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
		return marketplacecode + " " + preferenceCode ;
	}

}