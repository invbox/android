package com.ecinv.db.bo;

public class User {
	private String profileCode;
	private String profileName;
	private String firstName;
	private String lastName;
	private String email;
	private String address;
	private boolean retainHistory;
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getProfileCode() {
		return profileCode;
	}

	public void setId(String profileCode) {
		this.profileCode = profileCode;
	}

	public String getProfileName() {
		return profileName;
	}

	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	public String getFirstname() {
		return firstName;
	}

	public void setfirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getlastName() {
		return lastName;
	}

	public void setlastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public boolean getRetainHistory() {
		return this.retainHistory;
	}

	public void setRetainHistory(boolean retainHistory) {
		this.retainHistory = retainHistory;
	}

	// Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
		return firstName + " " + lastName;
	}

}