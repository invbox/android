package com.ecinv.db.bo;

public class Category {

	String categorycode;
	String categoryname;
	String categorydescription;
//	 createdby, creationdate, lastmodifiedby, lastmodifieddate
	public String getCategorycode() {
		return categorycode;
	}
	public void setCategorycode(String categorycode) {
		this.categorycode = categorycode;
	}
	public String getCategoryname() {
		return categoryname;
	}
	public void setCategoryname(String categoryname) {
		this.categoryname = categoryname;
	}
	public String getCategorydescription() {
		return categorydescription;
	}
	public void setCategorydescription(String categorydescription) {
		this.categorydescription = categorydescription;
	}
	public String toString(){
		return this.categoryname;
	}
}
