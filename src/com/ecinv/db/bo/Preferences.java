package com.ecinv.db.bo;

public class Preferences {

	private String marketplacecode;
	private String marketplacename;
	private String marketplacedescription;
	private String website;
	private String email;
	private boolean checked;
	private boolean prevState;
	private String preferenceCode;
	

	public String getMarketplacecode() {
		return marketplacecode;
	}


	public void setMarketplacecode(String marketplacecode) {
		this.marketplacecode = marketplacecode;
	}


	public String getMarketplacename() {
		return marketplacename;
	}


	public void setMarketplacename(String marketplacename) {
		this.marketplacename = marketplacename;
	}


	public String getMarketplacedescription() {
		return marketplacedescription;
	}


	public void setMarketplacedescription(String marketplacedescription) {
		this.marketplacedescription = marketplacedescription;
	}


	public String getWebsite() {
		return website;
	}


	public void setWebsite(String website) {
		this.website = website;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public boolean isChecked() {
		return checked;
	}


	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public void toggleChecked(){
		checked = !checked;
	}

	public boolean isPrevState() {
		return prevState;
	}


	public void setPrevState(boolean prevState) {
		this.prevState = prevState;
	}


	public String getPreferenceCode() {
		return preferenceCode;
	}


	public void setPreferenceCode(String preferenceCode) {
		this.preferenceCode = preferenceCode;
	}


	// Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
		return marketplacename;
	}

}