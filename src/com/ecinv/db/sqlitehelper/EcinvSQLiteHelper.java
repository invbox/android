package com.ecinv.db.sqlitehelper;

import com.invbox.R;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class EcinvSQLiteHelper extends SQLiteOpenHelper {

	private Context context = null;

	public static final String[] TABLES = { "UserProfile", "History", "item",
			"Category", "MarketPlace", "UserPreferences" };
	public static final String[] allUserColumns = { "profileCode",
			"profileName", "firstName", "lastName", "email", "address",
			"retainHistory", "password" };
	public static final String[] allHistoryColumns = { "historyCode",
			"profileCode", "itemCode", "listPrice", "marketPlaceCode",
			"searchDate" };
	public static final String[] allItemColumns = { "id", "profileCode",
			"itemCode", "itemDescription", "image", "itemName",
			"purchasePrice", "categoryCode", "type", "status", "serverId",
			"creationDate", "createdBy", "lastModifiedBy", "lastModifiedDate",
			"purchasedate" };
	public static final String[] allMarketPlaceColumns = { "marketplacecode",
			"marketplacename", "marketplacedescription", "website", "email" };
	public static final String[] allUserPreferencesColumns = {
			"preferenceCode", "profileCode", "marketPlaceCode" };
	public static final String[] allCategoryColumns = { "categorycode",
			"categoryname", "categorydescription"
	// , "createdby", "creationdate",
	// "lastmodifiedby", "lastmodifieddate"
	};

	public static final String TABLE_USER = "UserProfile";
	public static final String USERCOLUMN_USERID = "profileCode";
	public static final String USERCOLUMN_PROFILE_NAME = "profileName";
	public static final String USERCOLUMN_FIRSTNAME = "firstName";
	public static final String USERCOLUMN_LASTNAME = "lastName";
	public static final String USERCOLUMN_EMAIL = "email";
	public static final String USERCOLUMN_ADDRESS = "address";
	public static final String USERCOLUMN_RETAIN_HISTORY = "retainHistory";
	public static final String USERCOLUMN_PASSWORD = "password";

	public static final String TABLE_HISTORY = "History";
	public static final String HISTORYCOLUMN_ID = "historyCode";
	public static final String HISTORYCOLUMN_USERID = "profileCode";
	public static final String HISTORYCOLUMN_ITEM_CODE = "itemCode";
	public static final String HISTORYCOLUMN_LIST_PRICE = "listPrice";
	public static final String HISTORYCOLUMN_MARKET_CODE = "marketPlaceCode";
	public static final String HISTORYCOLUMN_SEARCHDATE = "searchDate";

	public static final String TABLE_ITEM = "Item";
	public static final String ITEMCOLUMN_ID = "id";
	public static final String ITEMCOLUMN_USERID = "profilecode";
	public static final String ITEMCOLUMN_ITEMCODE = "itemcode";
	public static final String ITEMCOLUMN_DESCRIPTION = "itemdescription";
	public static final String ITEMCOLUMN_IMAGE = "image";
	public static final String ITEMCOLUMN_ITEMNAME = "itemname";
	public static final String ITEMCOLUMN_PURCHASEPRICE = "purchaseprice";
	public static final String ITEMCOLUMN_CATEGORY = "categorycode";
	public static final String ITEMCOLUMN_TYPE = "type";
	// add status for "bought/sold"
	public static final String ITEMCOLUMN_STATUS = "status";
	public static final String ITEMCOLUMN_SERVERID = "serverid";
	public static final String ITEMCOLUMN_CREATION_DATE = "creationdate";
	public static final String ITEMCOLUMN_LAST_MODIFIED_DATE = "lastmodifieddate";
	public static final String ITEMCOLUMN_CREATED_BY = "createdby";
	public static final String ITEMCOLUMN_LAST_MODIFIED_BY = "lastmodifiedby";
	public static final String ITEMCOLUMN_PURCHASE_DATE = "purchasedate";

	public static final String TABLE_MARKETPLACE = "MarketPlace";
	public static final String MARKETPLACE_MARKETPLACECODE = "marketplacecode";
	public static final String MARKETPLACE_MARKETPLACENAME = "marketplacename";
	public static final String MARKETPLACE_MARKETPLACEDESCRIPTION = "marketplacedescription";
	public static final String MARKETPLACE_DESCRIPTION = "description";
	public static final String MARKETPLACE_WEBSITE = "website";
	public static final String MARKETPLACE_EMAIL = "email";

	public static final String TABLE_USERPREFERENCES = "UserPreferences";
	public static final String USERPREFERENCES_PREFERENCECODE = "preferencecode";
	public static final String USERPREFERENCES_MARKETPLACECODE = "marketplacecode";
	public static final String USERPREFERENCES_USERID = "profilecode";

	public static final String TABLE_CATEGORY = "CATEGORY";
	public static final String CATEGORY_CATEGORYCODE = "categorycode";
	public static final String CATEGORY_CATEGORYNAME = "categoryname";
	public static final String CATEGORY_CATEGORYDESCRIPTION = "categorydescription";

	private static final String DATABASE_NAME = "invbox.db";
	private static final int DATABASE_VERSION = 1;

	// Database creation sql statement
	private static final String[] CREATE_TABLES = {
			"create table UserProfile (profileCode TEXT PRIMARY KEY, profileName TEXT, firstname TEXT, lastname TEXT, email TEXT, address TEXT, retainHistory Integer, password TEXT);",
			"create table History (historyCode INTEGER PRIMARY KEY AUTOINCREMENT, profileCode TEXT, itemCode TEXT, listPrice REAL, marketPlaceCode TEXT, searchDate REAL, FOREIGN KEY(profileCode) REFERENCES userprofile (profilecode ));",
			"create table Item (id INTEGER PRIMARY KEY AUTOINCREMENT, serverid INTEGER , itemcode INTEGER, itemname TEXT, itemdescription TEXT, purchaseprice FLOAT, image BLOB, categorycode INTEGER, type TEXT, status TEXT, createdby TEXT, creationdate REAL, lastmodifiedby TEXT, lastmodifieddate REAL, profilecode INTEGER, purchasedate REAL, FOREIGN KEY(categorycode) REFERENCES category (categorycode ), FOREIGN KEY(profilecode) REFERENCES userprofile (profilecode ));",
			"create table Category (categorycode INTEGER PRIMARY KEY AUTOINCREMENT, categoryname TEXT, categorydescription TEXT, createdby TEXT, creationdate REAL, lastmodifiedby TEXT, lastmodifieddate REAL);",
			"create table MarketPlace (marketplacecode INTEGER,marketplacename TEXT ,marketplacedescription TEXT ,website TEXT ,email TEXT ,PRIMARY KEY (marketplacecode) )",
			"create table UserPreferences ( preferenceCode INTEGER PRIMARY KEY AUTOINCREMENT, profileCode INTEGER, marketPlaceCode INTEGER, FOREIGN KEY(marketPlaceCode) REFERENCES MarketPlace (marketPlaceCode), FOREIGN KEY(profileCode) REFERENCES UserProfile (profileCode) )" };

	public EcinvSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		for (int count = 0; count < CREATE_TABLES.length; count++) {
			database.execSQL(CREATE_TABLES[count]);
		}

		populateSearchSites(database);
		populateCategories(database);

	}

	private void populateSearchSites(SQLiteDatabase database) {

		String[] siteNames = this.context.getResources().getStringArray(
				R.array.sites);
		String[] siteDescs = this.context.getResources().getStringArray(
				R.array.sitesDesc);
		String[] siteWebsites = this.context.getResources().getStringArray(
				R.array.sitesWebsite);
		String[] siteEmails = this.context.getResources().getStringArray(
				R.array.sitesEmail);

		for (int count = 0; count < siteNames.length; count++) {
			SQLiteStatement statement = database
					.compileStatement("insert into MarketPlace(marketplacecode,marketplacename,marketplacedescription,website,email) values("
							+ (count + 1)
							+ ",'"
							+ siteNames[count]
							+ "','"
							+ siteDescs[count]
							+ "','"
							+ siteWebsites[count]
							+ "','" + siteEmails[count] + "')");
			statement.executeInsert();
		}
	}

	private void populateCategories(SQLiteDatabase database) {

		String[] categories = this.context.getResources().getStringArray(
				R.array.categories);

		for (int count = 0; count < categories.length; count++) {
			SQLiteStatement statement = database
					.compileStatement("insert into Category(categorycode, categoryname, categorydescription, createdby, creationdate, lastmodifiedby, lastmodifieddate ) values("
							+ (count + 1)
							+ ",'"
							+ categories[count]
							+ "','"
							+ categories[count] + "','','','','')");
			statement.executeInsert();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(EcinvSQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		for (int count = 0; count < TABLES.length; count++) {
			// db.execSQL("DROP TABLE IF EXISTS " + TABLES[count]);
		}
		onCreate(db);
	}

}