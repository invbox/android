package com.ecinv.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.AlertDialog;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ecinv.db.bo.Item;
import com.ecinv.db.datasource.CategoryDataSource;
import com.ecinv.db.datasource.ItemDataSource;
import com.ecinv.util.Util;
import com.invbox.R;
import com.paypal.android.MEP.PayPal;

public class MarketActivity extends ExpandableListActivity {

	private ItemDataSource itemDS;
	private CategoryDataSource categoryDS;
	private List<Item> items;
	private Context context;
	private HashMap<String, List<Item>> allItems = new HashMap<String, List<Item>>();
	private View currentView;
	private Resources resources;
	public static final int BuyThruPaypal = Menu.FIRST;
	private static PayPal paypalInstance = null;

	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.mkt);
			this.context = this.getApplicationContext();
			this.resources = context.getResources();
			refresh();
		} catch (Exception e) {
		}
	}

	public void refresh() {
		ItemExpandableListAdapter expListAdapter = new ItemExpandableListAdapter();
		setListAdapter(expListAdapter);
	}

	OnClickListener selectClickListener = new OnClickListener() {

		public void onClick(View v) {
			try {
				String[] tag = ((String) v.getTag()).split("_");
				String listName = tag[0];
				int pos = Integer.parseInt(tag[1]);
				if (listName == null || "".equalsIgnoreCase(listName))
					return;

				List<Item> list = allItems.get(listName);
				showListingDetailDialog(list.get(pos));

			} catch (Exception x) {
				x.printStackTrace();
			}
		}
	};

	private String[][] createChildArray() {

		String[][] strArr = new String[2][];

		List<Item> buyList = getItemsForStatus("buy");
		if (buyList != null) {
			strArr[0] = new String[buyList.size()];
		}
		Iterator<Item> iterator = buyList.iterator();
		int count = 0;
		while (iterator.hasNext()) {
			strArr[0][count++] = iterator.next().getItemname();
		}
		allItems.put("buy", buyList);

		List<Item> sellList = getItemsForStatus("sell");
		if (sellList != null) {
			strArr[1] = new String[sellList.size()];
		}
		iterator = sellList.iterator();
		count = 0;
		while (iterator.hasNext()) {
			strArr[1][count++] = iterator.next().getItemname();
		}
		allItems.put("sell", sellList);
		return strArr;
	}

	private List<Item> getItemsForStatus(String type) {
		if (itemDS == null) {
			itemDS = new ItemDataSource(this);
		}
		if (categoryDS == null) {
			categoryDS = new CategoryDataSource(this);
		}
		categoryDS.open();
		itemDS.open();

		try {
			items = itemDS.getItemsForTypesAndStatuses(new String[] { type },
					new String[] { "market" }, categoryDS);
		} finally {
			categoryDS.close();
			itemDS.close();
		}
		return items;
	}

	android.content.DialogInterface.OnClickListener onListingDetailDialogCloseListener = new android.content.DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
		}
	};

	private void showListingDetailDialog(Item item) {
		try {
			AlertDialog listingDetailDialog;
			AlertDialog.Builder builder = new AlertDialog.Builder(this);

			LinearLayout ll = new LinearLayout(this.context);
			ll.setOrientation(LinearLayout.VERTICAL);
			builder.setTitle(item.getItemname());
			builder.setPositiveButton("Close",
					onListingDetailDialogCloseListener);
			item.getImage();

			TextView itemCodeView = new TextView(this.context);
			ll.addView(itemCodeView);

			TextView itemNameView = new TextView(this.context);
			ll.addView(itemNameView);

			TextView itemDescView = new TextView(this.context);
			ll.addView(itemDescView);

			TextView itemPurPriceView = new TextView(this.context);
			ll.addView(itemPurPriceView);

			TextView itemTypeView = new TextView(this.context);
			ll.addView(itemTypeView);

			TextView itemStatusView = new TextView(this.context);
			ll.addView(itemStatusView);

			builder.setView(ll);
			itemCodeView.setText(Html.fromHtml("<b>Code:</b>&nbsp;&nbsp;"
					+ item.getItemCode()));
			itemNameView.setText(Html.fromHtml("<b>Name:</b>&nbsp;&nbsp;"
					+ item.getItemname()));
			itemDescView.setText(Html
					.fromHtml("<b>Description:</b>&nbsp;&nbsp;"
							+ item.getDescription()));
			itemPurPriceView.setText(Html.fromHtml("<b>Price:</b>&nbsp;&nbsp;"
					+ item.getPurchasePrice()));
			itemTypeView.setText(Html.fromHtml("<b>Type:</b>&nbsp;&nbsp;"
					+ item.getType()));
			itemStatusView.setText(Html.fromHtml("<b>Status:</b>&nbsp;&nbsp;"
					+ item.getStatus()));
			listingDetailDialog = builder.create();

			// show the dialog
			listingDetailDialog.show();
		} catch (Exception x) {
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (itemDS == null)
			itemDS = new ItemDataSource(this);
		refresh();
	}

	@Override
	protected void onPause() {
		if (itemDS != null)
			itemDS.close();
		super.onPause();
	}

	/**
	 * A simple adapter which maintains an ArrayList of photo resource Ids. Each
	 * photo is displayed as an image. This adapter supports clearing the list
	 * of photos and adding a new photo.
	 * 
	 */
	class ItemExpandableListAdapter extends BaseExpandableListAdapter {
		// Sample data set. children[i] contains the children (String[]) for
		// groups[i].
		ItemExpandableListAdapter() {
			inflater = LayoutInflater.from(context);
		}

		private String[] groups = { "Buy List", "Sell List" };
		private String[][] children = createChildArray();
		private LayoutInflater inflater;

		public Object getChild(int groupPosition, int childPosition) {
			return children[groupPosition][childPosition];
		}

		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}

		public int getChildrenCount(int groupPosition) {
			int i = 0;
			try {
				i = children[groupPosition].length;

			} catch (Exception e) {
			}

			return i;
		}

		public TextView getGenericView() {

			AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
					ViewGroup.LayoutParams.FILL_PARENT, 64);

			TextView textView = new TextView(context);
			textView.setLayoutParams(lp);
			// Center the text vertically
			textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
			textView.setTextColor(Color.BLUE);

			// Set the text starting position
			// independent of device
			int dips = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 36, getResources()
							.getDisplayMetrics());
			textView.setPadding(dips, 0, 0, 0);
			return textView;

		}

		private int getItemCountForSellWithItemCode(String itemCode, String type) {
			String result = null;
			InputStream is = null;

			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("itemcode", itemCode));
			nameValuePairs.add(new BasicNameValuePair("userid",
					LoginActivity.userid));
			nameValuePairs.add(new BasicNameValuePair("type", type));

			try {
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost("http://" + Util.SERVER_IP
						+ "/invbox/item/getItemsCountForItemCode");
				httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				httpPost.setHeader("Content-Type",
						"application/x-www-form-urlencoded");
				HttpResponse response = httpclient.execute(httpPost);
				HttpEntity entity = response.getEntity();
				is = entity.getContent();
				result = convertStreamToString(is);

				if (result != null && !"".equals(result)) {
					return Integer.parseInt(result.trim());
				}
				return 0;

			} catch (Exception e) {
				Log.e("HTTP", "Error in http connection " + e.toString());
				// return -1;
			}

			return 0;
		}

		public View getChildView(int groupPosition, int childPosition,
				boolean isLastChild, View convertView, ViewGroup parent) {

			convertView = inflater.inflate(R.layout.market, null);
			TextView textView = (TextView) convertView
					.findViewById(R.id.reportsSummary);

			Button mktButton = (Button) convertView
					.findViewById(R.id.mktPrice_btn);
			mktButton.setOnClickListener(marketClickListener);

			List<Item> itemList = allItems.get((groupPosition == 0 ? "buy"
					: "sell"));
			if (itemList == null)
				return null;

			textView.setText(getChild(groupPosition, childPosition).toString()
					+ "\n\n"
					+ getItemCountForSellWithItemCode(
							itemList.get(childPosition).getItemCode(), "buy")
					+ " buyer(s)   "
					+ getItemCountForSellWithItemCode(
							itemList.get(childPosition).getItemCode(), "sell")
					+ " seller(s)");
			textView.setTag((groupPosition == 0 ? "buy" : "sell") + "_"
					+ childPosition + "_"
					+ itemList.get(childPosition).getItemCode());
			mktButton.setTag((groupPosition == 0 ? "buy" : "sell") + "_"
					+ childPosition + "_"
					+ itemList.get(childPosition).getItemCode());
			/*
			 * if (groupPosition == 0) { registerForContextMenu(textView);
			 * textView.setOnLongClickListener(selectLongClickListener); }
			 */

			textView.setOnClickListener(selectClickListener);

			return convertView;
		}

		OnClickListener marketClickListener = new OnClickListener() {

			public void onClick(View v) {
				String[] tag = ((String) v.getTag()).split("_");
				String listName = tag[0];
				String searchTerm = tag[2];

				Intent tableLayout = new Intent(v.getContext(),
						TableLayoutActivity.class);
				tableLayout.putExtra("searchTerm", searchTerm);
				tableLayout.putExtra("callFrom", "market");

				startActivity(tableLayout);
			}
		};

		public Object getGroup(int groupPosition) {
			return groups[groupPosition];
		}

		public int getGroupCount() {
			return groups.length;
		}

		public long getGroupId(int groupPosition) {
			return groupPosition;
		}

		public View getGroupView(int groupPosition, boolean isExpanded,
				View convertView, ViewGroup parent) {
			TextView textView = getGenericView();
			textView.setText(getGroup(groupPosition).toString());
			return textView;
		}

		public boolean isChildSelectable(int groupPosition, int childPosition) {
			return true;
		}

		public boolean hasStableIds() {
			return true;
		}

	}

	private static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append((line + "\n"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

}
