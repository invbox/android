package com.ecinv.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ecinv.db.bo.Preferences;
import com.ecinv.db.datasource.PreferencesDataSource;
import com.invbox.R;

public class PreferencesActivity extends Activity {

	private PreferencesDataSource preferencesDS;
	List<Preferences> preferences;
	private Context context;
	private Button saveBtn;
	private Button closeBtn;
	public static final int PlaceInMarket = Menu.FIRST;

	private ListView mainListView;
	private ArrayAdapter<Preferences> listAdapter;

	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.preferencesmain);

			saveBtn = (Button) findViewById(R.id.prefSaveBtn);
			saveBtn.setTag(new Integer("1"));

			closeBtn = (Button) findViewById(R.id.prefCloseBtn);
			closeBtn.setTag(new Integer("2"));

			this.saveBtn.setOnClickListener(selectClickListener);
			this.closeBtn.setOnClickListener(selectClickListener);

			mainListView = (ListView) findViewById(R.id.preferenceListView);

			mainListView
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							Preferences preferences = listAdapter
									.getItem(position);
							preferences.toggleChecked();
							ItemViewHolder viewHolder = (ItemViewHolder) view
									.getTag();
							viewHolder.getCheckBox().setChecked(
									preferences.isChecked());
						}
					});
			setPreferencesForView();

		} catch (Exception x) {
			// Log.e(TagHandler, "onCreate", x);
			// this.showErrorDialog(x);
		}
	}

	/*
	 * private void removeView(List<Preferences> list) { for (int count = 0;
	 * count < list.size(); count++) { listAdapter.remove(list.get(count)); } }
	 */
	private void setPreferencesForView() {
		if (preferencesDS == null) {
			preferencesDS = new PreferencesDataSource(this);
		}
		preferencesDS.open();
		this.preferences = preferencesDS
				.getPreferencesForUser(LoginActivity.userid);

		listAdapter = new PreferencesArrayAdapter(this, this.preferences);
		mainListView.setAdapter(listAdapter);
		// refreshView(items);
	}

	OnClickListener selectClickListener = new OnClickListener() {

		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (((Integer) v.getTag()).intValue()) {

			case 1: {// save
				ArrayList<Preferences> tItems = new ArrayList<Preferences>();
				int multiSelect = 0;
				for (int count = 0; count < listAdapter.getCount(); count++) {
					Preferences preferences = listAdapter.getItem(count);
					if (preferences.isPrevState() == false
							&& preferences.isChecked()) {
						preferencesDS.addUserPreference(""
								+ preferences.getMarketplacecode());
					} else if (preferences.isPrevState()
							&& (preferences.isPrevState() != preferences
									.isChecked())) {
						// update that record
						preferencesDS.updateUserPreference(
								preferences.getMarketplacecode(),
								preferences.getPreferenceCode());
						preferences.setPrevState(!preferences.isPrevState());
					}
				}

				break;
			}
			case 2: {// close
				finish();
				break;
			}
			}
		}
	};

	private boolean isFieldValid() {
		return true;
	}

	private class PreferencesArrayAdapter extends ArrayAdapter<Preferences> {

		private LayoutInflater inflater;

		public PreferencesArrayAdapter(Context context,
				List<Preferences> preferenceList) {
			super(context, R.layout.preferences, R.id.siteLogo, preferenceList);
			inflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Preferences preferences = (Preferences) this.getItem(position);

			// The child views in each row.
			CheckBox checkBox;
			TextView textView;
			ImageView imageView;

			// Create a new row view
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.preferences, null);

				// Find the child views.
				imageView = (ImageView) convertView.findViewById(R.id.siteLogo);
				int resId = -1;
				String siteName = preferences.getMarketplacename();
				if ("ebay".equalsIgnoreCase(siteName)) {
					resId = R.drawable.ebay;
				} else if ("google".equalsIgnoreCase(siteName)) {
					resId = R.drawable.google;
				} else if ("invbox".equalsIgnoreCase(siteName)) {
					resId = R.drawable.invbox;
				}else if ("amazon".equalsIgnoreCase(siteName)) {
					resId = R.drawable.amazon;
				}
				imageView.setImageResource(resId);

				checkBox = (CheckBox) convertView
						.findViewById(R.id.preferenceRowCheckBox);

				checkBox.setChecked(preferences.isChecked());

				// Optimization: Tag the row with it's child views, so we don't
				// have to
				// call findViewById() later when we reuse the row.
				convertView.setTag(new ItemViewHolder(imageView, checkBox));

				// If CheckBox is toggled, update the preferences it is tagged
				// with.
				checkBox.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						CheckBox cb = (CheckBox) v;
						Preferences preferences = (Preferences) cb.getTag();
						preferences.setChecked(cb.isChecked());
					}
				});
			}
			// Reuse existing row view
			else {
				// Because we use a ViewHolder, we avoid having to call
				// findViewById().
				ItemViewHolder viewHolder = (ItemViewHolder) convertView
						.getTag();
				checkBox = viewHolder.getCheckBox();
				imageView = viewHolder.getImageView();
			}

			// Tag the CheckBox with the Planet it is displaying, so that we can
			// access the planet in onClick() when the CheckBox is toggled.
			checkBox.setTag(preferences);

			// Display planet data
			// checkBox.setChecked(preferences.isChecked());
			// textView.setText(preferences.getMarketplacename());

			return convertView;
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (preferencesDS == null) {
			preferencesDS = new PreferencesDataSource(this);
		}
		preferencesDS.open();
		setPreferencesForView();
		Log.w("onresume", "onresume");
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		preferencesDS.close();
		super.onPause();
	}

	/** Holds child views for one row. */
	private static class ItemViewHolder {
		private CheckBox checkBox;
		private ImageView imageView;

		public ItemViewHolder() {
		}

		public ItemViewHolder(ImageView imageView, CheckBox checkBox) {
			this.checkBox = checkBox;
			this.imageView = imageView;
		}

		public CheckBox getCheckBox() {
			return checkBox;
		}

		public void setCheckBox(CheckBox checkBox) {
			this.checkBox = checkBox;
		}

		public ImageView getImageView() {
			return imageView;
		}

		public void setImageView(TextView textView) {
			this.imageView = imageView;
		}
	}
}
