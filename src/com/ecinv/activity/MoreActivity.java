package com.ecinv.activity;

import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.ecinv.db.bo.Item;
import com.ecinv.db.datasource.ItemDataSource;
import com.invbox.R;

public class MoreActivity extends Activity {

	private ItemDataSource itemDS;
	private List<Item> items;
	private Context context;
	private HashMap<String, List<Item>> allItems = new HashMap<String, List<Item>>();
	static final String[] moreList = new String[] { "Preferences", "MyProfile",
			"Wish List", "History" };

	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.more);

			this.context = this.getApplicationContext();

			ListView listView = (ListView) findViewById(R.id.moreList);
			listView.setAdapter(new MySimpleArrayAdapter(this.context, moreList));

		} catch (Exception e) {
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		// if (itemDS == null)
		// itemDS = new ItemDataSource(this);
	}

	@Override
	protected void onPause() {
		// if (itemDS != null)
		// itemDS.close();
		super.onPause();
	}

	OnClickListener selectClickListener = new OnClickListener() {

		public void onClick(View v) {
			switch (((Integer) v.getTag()).intValue()) {

			case 0: {
				Intent preferencesIntent = new Intent(v.getContext(),
						PreferencesActivity.class);
				startActivity(preferencesIntent);
				break;
			}
			case 1: {
				break;
			}
			case 2: {
				Intent wishListIntent = new Intent(v.getContext(),
						WishListActivity.class);
				// wishListIntent.putExtra("edit", "false");
				startActivity(wishListIntent);
				break;
			}
			case 3: {
				Intent historyIntent = new Intent(v.getContext(),
						HistoryActivity.class);
				startActivity(historyIntent);
				break;
			}
			}

		}
	};

	public class MySimpleArrayAdapter extends ArrayAdapter<String> {
		private final Context context;
		private final String[] values;
		private LayoutInflater inflater;
		private TextView textView;

		public MySimpleArrayAdapter(Context context, String[] values) {
			super(context, R.layout.moredetails, R.id.moreText, values);
			this.context = context;
			this.values = values;
			inflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			convertView = inflater.inflate(R.layout.moredetails, null);
			textView = (TextView) convertView.findViewById(R.id.moreText);
			textView.setText(values[position]);
			textView.setTag(new Integer(position));
			textView.setOnClickListener(selectClickListener);
			String s = values[position];
			return convertView;
		}
	}

}
