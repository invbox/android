package com.ecinv.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.invbox.R;

public class TabsActivity extends TabActivity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.alltabs);

		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();

		TabHost tabHost = getTabHost();
		tabHost.setup();
		// Search Tab
		TabSpec spec1 = tabHost.newTabSpec("Tab 1");
		Intent searchIntent = new Intent(this, MyBarCodeScannerActivity.class);
		spec1.setContent(searchIntent);
		
		// spec1.setContent(R.id.tab1);
		tabHost.setBackgroundColor(Color.WHITE);
		spec1.setIndicator("Search",
				getResources().getDrawable(R.drawable.search));

		// Inventory Tab
		TabSpec spec2 = tabHost.newTabSpec("Tab 2");
		// spec2.setContent(R.id.tab2);
		Intent inventoryIntent = new Intent(this, InventoryActivity.class);
		spec2.setContent(inventoryIntent);
		spec2.setIndicator("Inventory",
				getResources().getDrawable(R.drawable.inventory));

		TabSpec spec3 = tabHost.newTabSpec("Tab 3");
		Intent marketIntent = new Intent(this, MarketActivity.class);
		spec3.setContent(marketIntent);
		spec3.setIndicator("@Market",
				getResources().getDrawable(R.drawable.market));

		TabSpec spec4 = tabHost.newTabSpec("Tab 4");
		Intent moreIntent = new Intent(this, MoreActivity.class);
		spec4.setContent(moreIntent);
		spec4.setIndicator("More");

		tabHost.addTab(spec1);
		tabHost.addTab(spec2);
		tabHost.addTab(spec3);
		tabHost.addTab(spec4);

		LayoutParams ll = new LinearLayout.LayoutParams(width / 4, height / 8);
		ll.gravity = Gravity.CENTER_VERTICAL;

		tabHost.getTabWidget().getChildAt(0)
				.setLayoutParams(new LinearLayout.LayoutParams(ll));
		tabHost.getTabWidget().getChildAt(1)
				.setLayoutParams(new LinearLayout.LayoutParams(ll));
		tabHost.getTabWidget().getChildAt(2)
				.setLayoutParams(new LinearLayout.LayoutParams(ll));
		tabHost.getTabWidget().getChildAt(3)
				.setLayoutParams(new LinearLayout.LayoutParams(ll));
	}

}