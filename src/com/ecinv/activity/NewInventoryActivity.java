package com.ecinv.activity;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ecinv.db.bo.Category;
import com.ecinv.db.bo.Item;
import com.ecinv.db.datasource.CategoryDataSource;
import com.ecinv.db.datasource.ItemDataSource;
import com.ecinv.scanner.IntentIntegrator;
import com.ecinv.scanner.IntentResult;
import com.ecinv.util.Util;
import com.invbox.R;

public class NewInventoryActivity extends Activity {

	private ItemDataSource itemDS;
	private CategoryDataSource categoryDS;
	private Resources resources;
	private Context context;
	private EditText input;
	private String purchasePrice;
	private String status;
	private String type;
	private boolean isEdit = false;
	private String serverid;
	private String id;

	private EditText itemCodeEdit;
	private EditText purchasePriceEdit;
	private EditText itemNameEdit;
	private EditText itemDescEdit;
	private EditText imageEdit;
	private DatePicker purchaseDatePicker;
	private Spinner categoryDropDown;
	private Uri imageUri;
	private final int SCAN_CALL = 0;
	private final int EDIT_CALL = 1;
	private final int PHOTO_CALL = 2;
	private final int BROWSE_CALL = 3;
	private ProgressDialog dialog;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newinventory);

		Bundle extras = getIntent().getExtras();

		String editVal;
		String iName;
		String iDesc;
		String iCode;
		String iPrice;
		String iDate;
		String iCategoryCode;

		if (extras != null) {
			editVal = extras.getString("edit");
			if (editVal != null && !"".equals(editVal)
					&& "true".equalsIgnoreCase(editVal)) {
				isEdit = true;
			}
		}

		this.resources = this.getResources();
		this.context = this.getApplicationContext();

		itemDS = new ItemDataSource(this);
		categoryDS = new CategoryDataSource(this);
		itemDS.open();
		categoryDS.open();

		Button saveBtn = (Button) findViewById(R.id.save_newInv);
		Button scanBtn = (Button) findViewById(R.id.scanBtn_newInv);
		Button photoBtn = (Button) findViewById(R.id.photoBtn_newInv);
		Button browseBtn = (Button) findViewById(R.id.browseBtn_newInv);

		purchasePriceEdit = (EditText) findViewById(R.id.price_newInv);
		itemNameEdit = (EditText) findViewById(R.id.itemName_newInv);
		itemCodeEdit = (EditText) findViewById(R.id.itemCode_newInv);
		itemDescEdit = (EditText) findViewById(R.id.itemDesc_newInv);
		imageEdit = (EditText) findViewById(R.id.url_newInv);
		purchaseDatePicker = (DatePicker) findViewById(R.id.purchaseDate_newInv);
		categoryDropDown = (Spinner) findViewById(R.id.categoryDropdown_newInv);

		categoryDropDown
				.setOnItemSelectedListener(new CustomOnItemSelectedListener());

		List<Category> list = categoryDS.getCategories();
		// List<String> strList = list.
		ArrayAdapter<Category> dataAdapter = new ArrayAdapter<Category>(this,
				android.R.layout.simple_spinner_item, list);
		// ArrayAdapter<Category> dataAdapter = new
		// ArrayAdapter<Category>(this,
		// R.id.categoryRowTextView, list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		categoryDropDown.setAdapter(dataAdapter);

		final IntentIntegrator integrator = new IntentIntegrator(this);

		saveBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				insertRow();
			}
		});

		scanBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				integrator.REQUEST_CODE = SCAN_CALL;
				integrator.initiateScan();
			}
		});

		browseBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

				startActivityForResult(i, BROWSE_CALL);

				/*
				 * Intent intent = new Intent(); intent.setType("image/*");
				 * intent.setAction(Intent.ACTION_GET_CONTENT);
				 * intent.addCategory(Intent.CATEGORY_OPENABLE);
				 * startActivityForResult(intent, BROWSE_CALL);
				 */
			}
		});

		photoBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				// define the file-name to save photo taken by Camera activity
				String fileName = "" + System.currentTimeMillis() + ".jpg";
				// create parameters for Intent with filename
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.TITLE, fileName);
				values.put(MediaStore.Images.Media.DESCRIPTION,
						"Image capture by camera");
				// imageUri is the current activity attribute,
				// define and save it for later usage (also in
				// onSaveInstanceState)
				imageUri = getContentResolver().insert(
						MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
				// create new Intent
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				File dir = Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
				File output = new File(dir, fileName);

				intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(output));

				// intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
				intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
				startActivityForResult(intent, PHOTO_CALL);

			}
		});

		if (isEdit) {
			saveBtn.setText("Edit");
			scanBtn.setVisibility(View.GONE);

			iName = extras.getString("itemName");
			itemNameEdit.setText(iName);

			iDesc = extras.getString("itemDesc");
			itemDescEdit.setText(iDesc);

			iCode = extras.getString("itemCode");
			itemCodeEdit.setText(iCode);

			iPrice = extras.getString("purchasePrice");
			purchasePriceEdit.setText(iPrice);

			iDate = extras.getString("purchaseDate");
			Date date = Util.convertStringToDate(iDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			purchaseDatePicker.init(cal.get(Calendar.YEAR),
					(cal.get(Calendar.MONTH) + 1),
					cal.get(Calendar.DAY_OF_MONTH), null);

			iCategoryCode = extras.getString("categoryCode");
			categoryDropDown.setSelection(Integer.parseInt(iCategoryCode));

			status = extras.getString("status");
			type = extras.getString("type");
			serverid = extras.getString("serverid");
			id = extras.getString("id");
		}
	}

	private String getPurchasePriceFromUser(String pname) {
		input = new EditText(NewInventoryActivity.this);

		new AlertDialog.Builder(NewInventoryActivity.this)
				.setTitle("Purchase Price")
				.setMessage("What was the purchase price of " + pname + "?")
				.setView(input)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Editable value = input.getText();
						purchasePrice = value.toString();
						purchasePriceEdit.setText(purchasePrice);
					}
				}).show();

		return null;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {

		switch (requestCode) {
		case SCAN_CALL: {
			IntentResult result = IntentIntegrator.parseActivityResult(
					requestCode, resultCode, intent);
			if (result != null) {
				String contents = result.getContents();
				if (contents != null) {
					itemCodeEdit = (EditText) findViewById(R.id.itemCode_newInv);
					itemCodeEdit.setText(contents);

					try {
						String pname = Util.getProductDetails(resources,
								context, contents);
						getPurchasePriceFromUser(pname);
						itemNameEdit.setText(pname);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {
					// showDialog(R.string.hello);
				}
			}
			break;
		}
		case PHOTO_CALL: {

			if (resultCode == RESULT_OK) {
				// use imageUri here to access the image

				Bundle extras = intent.getExtras();
				String test = "";
				if (extras != null) {
					test = extras.getString(MediaStore.EXTRA_OUTPUT);
				}
				imageEdit.setText(test);
			} else if (resultCode == RESULT_CANCELED) {
				// Toast.makeText(this, "Picture was not taken",
				// Toast.LENGTH_SHORT);
			} else {
				// Toast.makeText(this, "Picture was not taken",
				// Toast.LENGTH_SHORT);
			}

			break;
		}// end of case
		case BROWSE_CALL: {
			if (resultCode == Activity.RESULT_OK) {

				Uri selectedImage = intent.getData();
				String[] filePathColumn = { MediaStore.Images.Media.DATA };

				Cursor cursor = getContentResolver().query(selectedImage,
						filePathColumn, null, null, null);
				cursor.moveToFirst();

				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String picturePath = cursor.getString(columnIndex);
				imageEdit.setKeyListener(null);
				imageEdit.setText(picturePath);
				cursor.close();
			}

			break;
		}
		}// end of switch
	}

	public String getPath(Uri uri) {

		String[] projection = { MediaStore.Images.Media.DATA };
		Cursor cursor = managedQuery(uri, projection, null, null, null);

		int column_index = cursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();

		return cursor.getString(column_index);
	}

	private void insertRow() {

		EditText editField = (EditText) findViewById(R.id.itemName_newInv);
		String itemName = editField.getText().toString();

		editField = (EditText) findViewById(R.id.itemCode_newInv);
		String itemCode = editField.getText().toString();

		editField = (EditText) findViewById(R.id.itemDesc_newInv);
		String itemDesc = editField.getText().toString();

		editField = (EditText) findViewById(R.id.price_newInv);
		String purPrice = editField.getText().toString();

		DatePicker datePicker = (DatePicker) findViewById(R.id.purchaseDate_newInv);
		Date cal = new Date();
		cal.setDate(datePicker.getDayOfMonth());
		cal.setMonth(datePicker.getMonth());
		cal.setYear(datePicker.getYear());
		String purDate = "" + datePicker.getYear() + "-"
				+ datePicker.getMonth() + "-" + datePicker.getDayOfMonth()
				+ "T00:00:00Z";

		Spinner categorySelect = (Spinner) findViewById(R.id.categoryDropdown_newInv);
		int categoryCode = categorySelect.getSelectedItemPosition();

		Item item = new Item();
		item.setItemname(itemName);
		item.setItemCode(itemCode);
		item.setDescription(itemDesc);
		item.setPurchaseDate(purDate);
		item.setCategoryCode(categoryCode);
		String fileloc = imageEdit.getText().toString();
		String filename = fileloc.substring(fileloc.lastIndexOf("/") + 1);
		item.setImage("http://" + Util.SERVER_IP + "/invbox/images/items/"
				+ filename);
		item.setType(type);
		item.setStatus(status);
		item.setUserid(LoginActivity.getLoggedInUser());

		item.setPurchasePrice(new Double((purPrice != null && !""
				.equals(purPrice)) ? purPrice : "0"));

		if (itemDS == null) {
			itemDS = new ItemDataSource(this);
		}
		if (categoryDS == null) {
			categoryDS = new CategoryDataSource(this);
		}

		itemDS.open();
		categoryDS.open();

		if (!isEdit) {
			itemDS.addItem(LoginActivity.getLoggedInUser(), itemDesc, "http://"
					+ Util.SERVER_IP + "/invbox/images/items/" + filename,
					itemName, purPrice, purDate, itemCode, "sell", "inventory",
					"" + categoryCode);
			// send item to server.
		} else {
			item.setId(Long.parseLong(id));
			item.setServerId(serverid);
			itemDS.updateItem(item, LoginActivity.getLoggedInUser(), itemDesc,
					"http://" + Util.SERVER_IP + "/invbox/images/items/"
							+ filename, itemName, purPrice, purDate, itemCode,
					type, "inventory", "" + categoryCode);
		}

		if (fileloc != null && !"".equals(fileloc)) {
			uploadImageData(fileloc, "http://" + Util.SERVER_IP
					+ "/invbox/item/upload");
		}

		itemDS.close();
		categoryDS.close();
		// Intent inventoryIntent = new Intent(this, InventoryActivity.class);
		// startActivity(inventoryIntent);
		finish();

	}

	private void uploadImageData(String existingFileName, String urlString) {
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		File storageDir = null;
		HttpURLConnection conn;
		String filename = existingFileName.substring(existingFileName
				.lastIndexOf("/") + 1);
		try {
			// ------------------ CLIENT REQUEST

			int apiLevel = Integer.valueOf(android.os.Build.VERSION.SDK);

			// if (apiLevel <= 7) {
			// storageDir = Environment.getExternalStorageDirectory();
			// } else {
			// storageDir = Environment
			// .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
			// }

			// Log.e(Tag, "Inside second Method");

			FileInputStream fileInputStream = new FileInputStream(
					existingFileName);
			URL url = new URL(urlString);
			// Open a HTTP connection to the URL
			conn = (HttpURLConnection) url.openConnection();

			conn.setDoInput(true); // Allow Inputs
			conn.setDoOutput(true); // Allow Outputs
			conn.setUseCaches(false); // Don't use a Cached Copy
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setRequestProperty("ENCTYPE", "multipart/form-data");
			conn.setRequestProperty("Content-Type",
					"multipart/form-data;boundary=" + boundary);
			conn.setRequestProperty("uploaded_file", filename);
			DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

			dos.writeBytes(twoHyphens + boundary + lineEnd);
			dos.writeBytes("Content-Disposition: form-data; name=\"fileUpload\";filename=\""
					+ filename + "\"" + lineEnd);
			dos.writeBytes(lineEnd);

			// Log.e(Tag, "Headers are written");

			// create a buffer of maximum size

			int bytesAvailable = fileInputStream.available();
			int maxBufferSize = 1000;
			// int bufferSize = Math.min(bytesAvailable, maxBufferSize);
			byte[] buffer = new byte[bytesAvailable];

			// read file and write it into form...

			int bytesRead = fileInputStream.read(buffer, 0, bytesAvailable);

			while (bytesRead > 0) {
				dos.write(buffer, 0, bytesAvailable);
				bytesAvailable = fileInputStream.available();
				bytesAvailable = Math.min(bytesAvailable, maxBufferSize);
				bytesRead = fileInputStream.read(buffer, 0, bytesAvailable);
			}

			// send multipart form data necesssary after file data...

			dos.writeBytes(lineEnd);
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			int serverResponseCode = conn.getResponseCode();
			String serverResponseMessage = conn.getResponseMessage();

			// close streams
			// Log.e(Tag, "File is written");
			fileInputStream.close();
			dos.flush();
			dos.close();

		} catch (MalformedURLException ex) {
			Log.e("File_loading", "error: " + ex.getMessage(), ex);
		}

		catch (IOException ioe) {
			Log.e("File_loading", "error: " + ioe.getMessage(), ioe);
		}

		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser sp = null;
		try {
			sp = spf.newSAXParser();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Get the XMLReader of the SAXParser we created.
		XMLReader xr = null;
		try {
			xr = sp.getXMLReader();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		 * // Create a new ContentHandler and apply it to the XML-Reader
		 * MyExampleHandler1 myExampleHandler = new MyExampleHandler1();
		 * xr.setContentHandler(myExampleHandler);
		 * 
		 * // Parse the xml-data from our URL. try { xr.parse(new
		 * InputSource(conn.getInputStream())); //xr.parse(new InputSource(new
		 * java.io.FileInputStream(new java.io.File("login.xml")))); } catch
		 * (MalformedURLException e) { Log.d("Net Disconnected",
		 * "NetDisconeeted"); // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (IOException e) {
		 * Log.d("Net Disconnected", "NetDisconeeted"); // TODO Auto-generated
		 * catch block e.printStackTrace(); } catch (SAXException e) {
		 * Log.d("Net Disconnected", "NetDisconeeted"); // TODO Auto-generated
		 * catch block e.printStackTrace(); }
		 */
		// Parsing has finished.

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		if (itemDS == null) {
			itemDS = new ItemDataSource(this);
		}
		if (categoryDS == null) {
			categoryDS = new CategoryDataSource(this);
		}
		itemDS.open();
		categoryDS.open();

		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		itemDS.close();
		categoryDS.close();
		super.onPause();
	}

	private class CategoryArrayAdapter extends ArrayAdapter<Category> {

		private LayoutInflater inflater;

		public CategoryArrayAdapter(Context context, List<Category> categoryList) {
			super(context, R.layout.category, R.id.categoryRowTextView,
					categoryList);
			inflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Category category = (Category) this.getItem(position);

			// The child views in each row.
			// CheckBox checkBox;
			RadioButton radioButton;
			TextView textView;

			// Create a new row view
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.category, null);

				// Find the child views.
				textView = (TextView) convertView
						.findViewById(R.id.rowTextView);

				radioButton = (RadioButton) convertView
						.findViewById(R.id.categoryRadioButton);

				// Optimization: Tag the row with it's child views, so we don't
				// have to
				// call findViewById() later when we reuse the row.
				convertView.setTag(new ItemViewHolder(textView, radioButton));

				// If CheckBox is toggled, update the item it is tagged with.
				radioButton.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						CheckBox cb = (CheckBox) v;
						Item item = (Item) cb.getTag();
						item.setChecked(cb.isChecked());
					}
				});
			}
			// Reuse existing row view
			else {
				// Because we use a ViewHolder, we avoid having to call
				// findViewById().
				ItemViewHolder viewHolder = (ItemViewHolder) convertView
						.getTag();
				radioButton = viewHolder.getRadioButton();
				textView = viewHolder.getTextView();
			}

			// Tag the CheckBox with the category it is displaying, so that we
			// can
			// access the category in onClick() when the CheckBox is toggled.
			radioButton.setTag(category);

			// Display category data
			// checkBox.setChecked(category.isChecked());
			textView.setText(category.getCategoryname());

			return convertView;

		}

	}

	/** Holds child views for one row. */
	private static class ItemViewHolder {
		private TextView textView;
		private RadioButton radioButton;

		public ItemViewHolder() {
		}

		public ItemViewHolder(TextView textView, RadioButton radioButton) {
			this.radioButton = radioButton;
			this.textView = textView;
		}

		public TextView getTextView() {
			return textView;
		}

		public void setTextView(TextView textView) {
			this.textView = textView;
		}

		public RadioButton getRadioButton() {
			return radioButton;
		}

		public void setCheckBox(RadioButton radioButton) {
			this.radioButton = radioButton;
		}

		public String toString() {
			return this.textView.getText().toString();
		}
	}
}

class CustomOnItemSelectedListener implements OnItemSelectedListener {

	public void onItemSelected(AdapterView<?> parent, View view, int pos,
			long id) {
		view.setTag("" + pos);
		Toast.makeText(
				parent.getContext(),
				"OnItemSelectedListener : "
						+ parent.getItemAtPosition(pos).toString(),
				Toast.LENGTH_SHORT).show();
	}

	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}

}
