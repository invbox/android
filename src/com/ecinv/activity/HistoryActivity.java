package com.ecinv.activity;

import java.util.List;

import android.R.color;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.ecinv.db.bo.History;
import com.ecinv.db.datasource.HistoryDataSource;
import com.ecinv.db.datasource.ItemDataSource;
import com.invbox.R;

public class HistoryActivity extends Activity {

	private HistoryDataSource historyDS;
	private List<History> histories;
	private Context context;

	private ListView historyListView;
	private ArrayAdapter<History> listAdapter;

	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.history);

			historyListView = (ListView) findViewById(R.id.historyListView);

			historyListView
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							History history = listAdapter.getItem(position);
							ItemViewHolder viewHolder = (ItemViewHolder) view
									.getTag();
						}
					});
			setItemsForView();

		} catch (Exception x) {
			Log.w("onCreate", x);
			// this.showErrorDialog(x);
		}
	}

	private void removeView(List<History> list) {
		for (int count = 0; count < list.size(); count++) {
			listAdapter.remove(list.get(count));
		}
	}

	private void setItemsForView() {
		if (historyDS == null) {
			historyDS = new HistoryDataSource(this);
		}
		historyDS.open();
		histories = historyDS.getHistory();

		listAdapter = new HistoryArrayAdapter(this, histories);
		if (this.historyListView == null) {
			this.historyListView = (ListView) findViewById(R.id.historyListView);
			Log.w("historylistView",""+this.historyListView);
		}
		if (listAdapter != null) {
			Log.w("listAdapter",""+listAdapter);
			this.historyListView.setAdapter(listAdapter);
			
		}
	}

	OnClickListener selectClickListener = new OnClickListener() {

		public void onClick(View v) {
		}
	};

	private boolean isFieldValid() {
		return true;
	}

	
	private class HistoryArrayAdapter extends ArrayAdapter<History> {

		private LayoutInflater inflater;

		public HistoryArrayAdapter(Context context, List<History> historyList) {
			super(context, R.layout.history, R.id.historyRowTextView,
					historyList);
			inflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			History history = (History) this.getItem(position);

			// The child views in each row.
			// CheckBox checkBox;
			TextView textView;

			// Create a new row view
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.history, null);

				// Find the child views.
				textView = (TextView) convertView
						.findViewById(R.id.historyRowTextView);

				convertView.setTag(new ItemViewHolder(textView));
			}
			// Reuse existing row view
			else {
				ItemViewHolder viewHolder = (ItemViewHolder) convertView
						.getTag();
				textView = viewHolder.getTextView();
			}

			if (history == null)
				return convertView;
			textView.setText(history.getContent());
			textView.setTextColor(Color.BLUE);

			return convertView;
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if (historyDS == null) {
			historyDS = new HistoryDataSource(this);
		}
		historyDS.open();
//		setItemsForView();
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		historyDS.close();
		super.onPause();
	}

	/** Holds child views for one row. */
	private static class ItemViewHolder {
		private TextView textView;

		public ItemViewHolder() {
		}

		public ItemViewHolder(TextView textView) {
			// this.checkBox = checkBox;
			this.textView = textView;
		}

		public TextView getTextView() {
			return textView;
		}

		public void setTextView(TextView textView) {
			this.textView = textView;
		}
	}
}
