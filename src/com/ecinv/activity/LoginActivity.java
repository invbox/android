package com.ecinv.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ecinv.db.datasource.UserDataSource;
import com.ecinv.util.CryptographyUtil;
import com.invbox.R;

public class LoginActivity extends Activity {

	UserDataSource userDS;
	Context context;
	public static byte[] pKey;
	public static String userid;
	private static boolean SERVER_VERIFIED = false;

	public static boolean isUserServerVerified() {
		return SERVER_VERIFIED;
	}

	public static void setSERVER_VERIFIED(boolean sERVER_VERIFIED) {
		SERVER_VERIFIED = sERVER_VERIFIED;
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		context = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);

		Button submitBtn = (Button) findViewById(R.id.login_submitBtn);
		submitBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// check for user from server and db?
				EditText pwdView = (EditText) findViewById(R.id.pwdEditView);
				EditText useridView = (EditText) findViewById(R.id.emailEditView);
				String typedPwd = pwdView.getText().toString();
				String userid = useridView.getText().toString();

				if (typedPwd == null || "".equals(typedPwd) || userid == null
						|| "".equals(userid)) {
					Toast toast = Toast.makeText(context,
							"Please provide the valid userid and password.",
							Toast.LENGTH_LONG);
					toast.show();
					pwdView.setText("");
					return;
				}

				String localDbPwd = getPwdFromLocalDB(userid);

				if (localDbPwd == null || "".equals(localDbPwd)) {
					Toast toast = Toast
							.makeText(
									context,
									"You are not a user in the system. Please create a user using the below New User button.",
									Toast.LENGTH_LONG);
					toast.show();
					pwdView.setText("");
					return;
				}
				if (!isAuthenticatedUser(localDbPwd, typedPwd)) {
					Toast toast = Toast
							.makeText(
									context,
									"Please retype your userid(E-mailid) and password correctly, as you are not identified in the system.",
									Toast.LENGTH_LONG);
					toast.show();
					pwdView.setText("");
					return;
				}

				setUserid(userid);
				// server verification
				try {
					if (userDS.verifyUserCredentials(userid, typedPwd)) {
						setSERVER_VERIFIED(true);
					} else {
						Toast toast = Toast
								.makeText(
										context,
										"Please connect to the internet to access the invBox cloud services, but anyways you would be "
												+ "able to connect to the local device. Logout/Close the application and reconnect if you "
												+ "need the cloud services", Toast.LENGTH_LONG);
						toast.show();
						setSERVER_VERIFIED(false);
					}
				} catch (Exception e) {

				}
				Intent tableLayout = new Intent(v.getContext(),
						TabsActivity.class);
				startActivity(tableLayout);

			}
		});

		Button newUserBtn = (Button) findViewById(R.id.createUserBtn);
		newUserBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent tableLayout = new Intent(v.getContext(),
						CreateNewUserActivity.class);
				startActivity(tableLayout);
			}
		});
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.w("onresume", "onresumelogin");
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.w("onPause", "onPauselogin");
	}

	private void setUserid(String userid) {
		LoginActivity.userid = userid;
	}

	public static String getLoggedInUser() {
		return LoginActivity.userid;
	}

	private boolean isAuthenticatedUser(String localPwd, String typedPwd) {

		boolean isAuthenticated = false;
		try {
			if (CryptographyUtil.checkForAuthentication(localPwd, typedPwd)) {
				isAuthenticated = true;
			}
		} catch (Exception e) {
		}
		return isAuthenticated;
	}

	private String getPwdFromLocalDB(String userid) {

		if (userDS == null) {
			userDS = new UserDataSource(this);
		}
		userDS.open();
		String pwd = userDS.getUserPwdFromLocal(userid);

		userDS.close();

		return pwd;
	}
}