package com.ecinv.activity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

import com.ecinv.db.bo.Category;
import com.ecinv.db.bo.History;
import com.ecinv.db.datasource.CategoryDataSource;
import com.ecinv.db.datasource.HistoryDataSource;
import com.ecinv.db.datasource.ItemDataSource;
import com.ecinv.search.RequestInvoker;
import com.ecinv.search.ResponseParser;
import com.ecinv.util.Listing;
import com.ecinv.util.SearchResult;
import com.invbox.R;
import com.paypal.android.MEP.PayPal;

public class TableLayoutActivity extends Activity {

	private ImageView imageViewImage;
	private AlertDialog listingDetailDialog;
	private static ProgressDialog progressDialog;
	private final static String TAG = "ecInv";
	private static RequestInvoker invoker;
	private static ResponseParser parser;
	private static PayPal paypalInstance = null;

	private Context context;
	private String searchTerm = "786936284065";
	// "786936284065";
	private SearchResult listings;
	private Vector<SearchResult> allClients = new Vector<SearchResult>();
	private Listing selectedListing;
	private TextView textViewStartTime;
	private TextView textViewEndTime;
	private TextView textViewListingType;
	private TextView textViewPrice;
	private TextView textViewShipping;
	private TextView textViewLocation;
	private TextView textViewLink;
	private ItemDataSource itemDS;
	private View currentView;
	private Spinner categoryDropDown;

	public Resources resources;

	public static final int AddToInventory = Menu.FIRST;
	public static final int AddToWishList = Menu.FIRST + 1;
	public static final int PaypalCall = Menu.FIRST + 2;

	private int ID_DIALOG_SEARCHING = 1;
	private boolean DONE = false;
	private EditText input;
	private DatePicker datePicker;
	private String callFrom;
	private String previousSearchPrice;
	private String itemUPCCode;
	private CategoryDataSource categoryDS;

	List<Thread> threads = new ArrayList<Thread>();

	ProgressDialog dialog;
	int totalThreads = 0;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.resources = this.getResources();
		setContentView(R.layout.tablelayout);

		this.context = this.getApplicationContext();
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			searchTerm = extras.getString("searchTerm");
			callFrom = extras.getString("callFrom");
		}
		invokeApi();

		try {
			totalThreads = threads.size();
			for (int tCount = 0; tCount < threads.size(); tCount++) {
				((Thread) threads.get(tCount)).join();
			}
			Thread thread = new Thread(new RequestThread("amazon",
					this.context, handler));
			thread.setName("amazon" + "thread");
			this.threads.add(thread);
			thread.start();
			thread.join();

			thread = new Thread(
					new RequestThread("ebay", this.context, handler));
			thread.setName("ebay" + "thread");
			this.threads.add(thread);
			thread.start();

		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		itemDS = new ItemDataSource(this);
		itemDS.open();

		TableLayout tableView = null;
		try {
			dialog = ProgressDialog.show(this, "", "Loading..", true);
			tableView = this.getView();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * protected Dialog onCreateDialog(int id) { if (id == ID_DIALOG_SEARCHING)
	 * { ProgressDialog loadingDialog = new ProgressDialog(this);
	 * loadingDialog.setMessage("searching...");
	 * loadingDialog.setIndeterminate(true); loadingDialog.setCancelable(true);
	 * return loadingDialog; } return super.onCreateDialog(id); }
	 * 
	 * protected void onSaveInstanceState(Bundle outState) { try {
	 * dismissDialog(ID_DIALOG_SEARCHING); } catch (Exception e) { // ignore
	 * error } super.onSaveInstanceState(outState); }
	 */
	final Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			// Get the current value of the variable total from the message data
			// and update the progress bar.
			if (totalThreads <= 0) {
				dialog.dismiss();
			}
		}
	};

	public void onListItemClick(ListView l, View v, int position, long id) {
		l.showContextMenuForChild(v);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		itemDS.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		itemDS.close();
		super.onPause();
	}

	private void showErrorDialog(Exception x) {
		try {
			new AlertDialog.Builder(this).setTitle(R.string.app_name)
					.setMessage(x.getMessage())
					.setPositiveButton("Close", null).show();
		} catch (Exception reallyBadTimes) {
			Log.e(TAG, "showErrorDialog", reallyBadTimes);
		}
	}

	OnClickListener selectClickListener = new OnClickListener() {

		public void onClick(View v) {
			ArrayList<Listing> tempListings = null;
			try {
				String[] tag = ((String) v.getTag()).split("_");
				String siteName = tag[0];
				String pos = tag[1];
				for (int count = 0; count < allClients.size(); count++) {
					Iterator<Listing> iterator = allClients.get(count)
							.getListings().iterator();

					iterator.next();
					tempListings = allClients.get(count).getListings();
					if (siteName.equalsIgnoreCase(tempListings.get(0)
							.getSiteName())) {
						break;
					}

				}

				selectedListing = (Listing) tempListings.get(Integer
						.parseInt(pos));
				showListingDetailDialog();
			} catch (Exception x) {
				Log.e(TAG, "selectItemListener.onItemClick", x);
				x.printStackTrace();
			}
		}
	};

	OnLongClickListener selectLongClickListener = new OnLongClickListener() {

		public boolean onLongClick(View v) {
			// TODO Auto-generated method stub
			ArrayList<Listing> tempListings = null;
			try {
				String[] tag = ((String) v.getTag()).split("_");
				String siteName = tag[0];
				String pos = tag[1];
				for (int count = 0; count < allClients.size(); count++) {
					Iterator<Listing> iterator = allClients.get(count)
							.getListings().iterator();

					iterator.next();
					tempListings = allClients.get(count).getListings();
					if (siteName.equalsIgnoreCase(tempListings.get(0)
							.getSiteName())) {
						break;
					}

				}

				selectedListing = (Listing) tempListings.get(Integer
						.parseInt(pos));
			} catch (Exception x) {
				Log.e(TAG, "selectItemListener.onItemClick", x);
				x.printStackTrace();
			}
			return true;
		}

	};

	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.setHeaderTitle("Actions");
		if (!("market".equalsIgnoreCase(callFrom))) {
			menu.add(0, AddToInventory, 0, "Add to Inventory");
			menu.add(0, AddToWishList, 0, "Add to Wishlist");
		}
		if (("market".equalsIgnoreCase(callFrom))
				&& (((String) v.getTag()).split("_")[0]
						.equalsIgnoreCase("invbox"))) {
			menu.add(0, PaypalCall, 0, "Pay with Paypal");
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
				.getMenuInfo();

		if (item.getTitle() == "Add to Inventory") {
			addToInventory(currentView);
		} else if (item.getTitle() == "Add to Wishlist") {
			addToWishList(currentView);
		} else if (item.getTitle() == "Pay with Paypal") {
			createPaypalInstance(currentView);
		} else {
			return false;
		}

		return true;

	}

	private void createPaypalInstance(View v) {

		Context contxt = v.getContext();
		setItem(v);
		String serverType = getValueForElementForClient(
				"invbox_paypal_use_server").coerceToString().toString();
		String paypalId = "";
		int type = 1;
		if (serverType != null && !"".equalsIgnoreCase(serverType)) {
			if ("sandbox".equalsIgnoreCase(serverType)) {
				paypalId = getValueForElementForClient(
						"invbox_paypalid_sandbox").coerceToString().toString();
				type = 1;
			} else {
				paypalId = getValueForElementForClient(
						"invbox_paypalid_production").coerceToString()
						.toString();
				type = 2;
			}
		}

		if (TableLayoutActivity.paypalInstance == null) {
			TableLayoutActivity.paypalInstance = PayPal.initWithAppID(this
					.getBaseContext(), paypalId,
					((type == 1) ? PayPal.ENV_SANDBOX : PayPal.ENV_LIVE));
		}
		Intent paypalLayout = new Intent(this.context,
				InvboxPaypalActivity.class);

		String recepient = getValueForElementForClient(
				"invbox_paypal_recepient_email").coerceToString().toString();
		recepient.substring(recepient.indexOf("@")).replaceAll("_dot_", ".");
		paypalLayout.putExtra("price",
				"" + this.selectedListing.getCurrentPrice());
		paypalLayout.putExtra("id", "" + this.selectedListing.getId());
		paypalLayout.putExtra("recepient", recepient);
		startActivity(paypalLayout);
	}

	public static PayPal getPaypalInstance() {
		return TableLayoutActivity.paypalInstance;
	}

	private void setItem(View v) {
		ArrayList<Listing> tempListings = null;
		try {
			String[] tag = ((String) v.getTag()).split("_");
			String siteName = tag[0];
			String pos = tag[1];
			for (int count = 0; count < allClients.size(); count++) {
				Iterator<Listing> iterator = allClients.get(count)
						.getListings().iterator();

				iterator.next();
				tempListings = allClients.get(count).getListings();
				if (siteName
						.equalsIgnoreCase(tempListings.get(0).getSiteName())) {
					break;
				}
			}
			selectedListing = (Listing) tempListings.get(Integer.parseInt(pos));
		} catch (Exception x) {
			Log.e(TAG, "selectItemListener.onItemClick", x);
			x.printStackTrace();
		}

	}

	public void addToInventory(View v) {
		setItem(v);
		// need to add the lowest of all
		input = new EditText(TableLayoutActivity.this);
		datePicker = new DatePicker(TableLayoutActivity.this);
		Calendar cal = Calendar.getInstance();
		datePicker.init(cal.get(Calendar.YEAR), (cal.get(Calendar.MONTH)),
				cal.get(Calendar.DAY_OF_MONTH), null);

		categoryDS = new CategoryDataSource(context);
		categoryDS.open();

		categoryDropDown = new Spinner(this);

		categoryDropDown
				.setOnItemSelectedListener(new CustomOnItemSelectedListener());

		List<Category> list = categoryDS.getCategories();
		// List<String> strList = list.
		ArrayAdapter<Category> dataAdapter = new ArrayAdapter<Category>(
				context, android.R.layout.simple_spinner_item, list);
		selectedListing.setId(itemUPCCode);
		// ArrayAdapter<Category>
		// dataAdapter = new
		// ArrayAdapter<Category>(this,
		// R.id.categoryRowTextView,
		// list);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		categoryDropDown.setAdapter(dataAdapter);

		new AlertDialog.Builder(TableLayoutActivity.this)
				.setTitle("Purchase Date")
				.setView(datePicker)
				.setMessage(
						"When was the purchase date of "
								+ this.selectedListing.getTitle() + "?")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						String purDate = "" + datePicker.getYear() + "-"
								+ datePicker.getMonth() + "-"
								+ datePicker.getDayOfMonth() + "T00:00:00Z";
						selectedListing.setPurchaseDate(purDate);

						new AlertDialog.Builder(TableLayoutActivity.this)
								.setTitle("Category")
								.setView(categoryDropDown)
								.setMessage(
										"Choose the category "
												+ selectedListing.getTitle()
												+ " belongs to?")
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int whichButton) {
												int categoryCode = categoryDropDown
														.getSelectedItemPosition();
												selectedListing
														.setCategoryCode(""
																+ categoryCode);
												new AlertDialog.Builder(
														TableLayoutActivity.this)
														.setTitle(
																"Purchase Price")
														.setView(input)
														.setMessage(
																"What was the purchase price of "
																		+ selectedListing
																				.getTitle()
																		+ "?")
														.setPositiveButton(
																"OK",
																new DialogInterface.OnClickListener() {
																	public void onClick(
																			DialogInterface dialog,
																			int whichButton) {
																		Editable value = input
																				.getText();
																		selectedListing
																				.setPurchasePrice(value
																						.toString());
																		selectedListing
																				.setType("sell");
																		selectedListing
																				.setStatus("inventory");
																		itemDS.addItem(
																				selectedListing,
																				LoginActivity.userid);
																	}
																}).show();

											}
										}).show();
					}
				}).show();
	}

	public void addToWishList(View v) {
		// wishListDS.addToWishList("user1", "description", "itemcode", "image",
		// "itemname", "sell");
		setItem(v);
		// need to add the lowest of all
		input = new EditText(TableLayoutActivity.this);
		new AlertDialog.Builder(TableLayoutActivity.this)
				.setTitle("Buy Price")
				.setView(input)
				.setMessage(
						"What is your expected upper buy price of "
								+ this.selectedListing.getTitle() + "?")
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						Editable value = input.getText();
						selectedListing.setPurchasePrice(value.toString());
						// selectedListing.setId(searchTerm);
						selectedListing.setId(selectedListing.getId());
						selectedListing.setType("buy");
						selectedListing.setStatus("market");
						itemDS.addItem(selectedListing, LoginActivity.userid);
					}
				}).show();
	}

	private void showListingDetailDialog() {
		try {
			// I don't think this can actually happen so this is just a sanity
			// check
			if (this.selectedListing == null) {
				return;
			}
			// create the listing detail dialog
			if (this.listingDetailDialog == null) {
				// LayoutInflater inflater = (LayoutInflater) this
				// .getSystemService(LAYOUT_INFLATER_SERVICE);
				// View layout = inflater
				// .inflate(
				// R.layout.listingdetail,
				// (ViewGroup) findViewById(R.id.listingdetaildialog_root));
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				// builder.setView(findViewById(R.layout.listingdetail));
				// View layout =
				// (View)findViewById(R.id.listingdetaildialog_root1);
				LinearLayout ll = new LinearLayout(this.context);
				ll.setOrientation(LinearLayout.VERTICAL);
				builder.setTitle(this.selectedListing.getTitle());
				builder.setPositiveButton("Close",
						onListingDetailDialogCloseListener);
				// this.imageViewImage = (ImageView)
				// findViewById(R.id.listingdetail_image);
				this.textViewStartTime = new TextView(this.context);
				ll.addView(this.textViewStartTime);
				this.textViewEndTime = new TextView(this.context);
				ll.addView(this.textViewEndTime);
				this.textViewListingType = new TextView(this.context);
				ll.addView(this.textViewListingType);
				this.textViewPrice = new TextView(this.context);
				ll.addView(this.textViewPrice);
				this.textViewShipping = new TextView(this.context);
				ll.addView(this.textViewShipping);
				this.textViewLocation = new TextView(this.context);
				ll.addView(this.textViewLocation);
				this.textViewLink = new TextView(this.context);
				ll.addView(this.textViewLink);
				builder.setView(ll);

				// this.imageViewImage = (ImageView)
				// findViewById(R.id.listingdetail_image);
				// this.textViewStartTime = (TextView)
				// findViewById(R.id.listingdetail_starttime);
				// this.textViewEndTime = (TextView)
				// findViewById(R.id.listingdetail_endtime);
				// this.textViewListingType = (TextView)
				// findViewById(R.id.listingdetail_listingtype);
				// this.textViewPrice = (TextView)
				// findViewById(R.id.listingdetail_price);
				// this.textViewShipping = (TextView)
				// findViewById(R.id.listingdetail_shipping);
				// this.textViewLocation = (TextView)
				// findViewById(R.id.listingdetail_location);
				// this.textViewLink = (TextView)
				// findViewById(R.id.listingdetail_link);
				this.listingDetailDialog = builder.create();
			}
			// set the values
			// this.textViewStartTime.setText(Html
			// .fromHtml("<b>Start Time:</b>&nbsp;&nbsp;"
			// + this.selectedListing.getStartTime()
			// .toLocaleString()));
			// this.textViewEndTime.setText(Html
			// .fromHtml("<b>End Time:</b>&nbsp;&nbsp;"
			// + this.selectedListing.getEndTime()
			// .toLocaleString()));
			this.textViewPrice.setText(Html
					.fromHtml("<b>Price:</b>&nbsp;&nbsp;"
							+ this.selectedListing.getCurrentPrice()));
			this.textViewShipping.setText(Html
					.fromHtml("<b>Shipping Cost:</b>&nbsp;&nbsp;"
							+ this.selectedListing.getShippingCost()));
			this.textViewLocation.setText(Html
					.fromHtml("<b>Location</b>&nbsp;&nbsp;"
							+ this.selectedListing.getLocation()));
			String listingType = new String("<b>Listing Type:</b>&nbsp;&nbsp;");
			if (this.selectedListing.isAuction()) {
				listingType = listingType + "Auction";
				if (this.selectedListing.isBuyItNow()) {
					listingType = listingType + ", " + "Buy it now";
				}
			} else if (this.selectedListing.isBuyItNow()) {
				listingType = listingType + "Buy it now";
			} else {
				listingType = listingType + "Not specified";
			}
			// url field
			this.textViewListingType.setText(Html.fromHtml(listingType));
			StringBuffer html = new StringBuffer("<a href='");
			html.append(this.selectedListing.getListingUrl());
			html.append("'>");
			html.append("View original listing on ");
			html.append(this.selectedListing.getAuctionSource());
			html.append("</a>");
			this.textViewLink.setText(Html.fromHtml(html.toString()));
			this.textViewLink.setOnClickListener(urlClickedListener);
			// set the image
			Bitmap bitmap = getImageFromURL(this.selectedListing.getImageUrl());
			this.imageViewImage.setImageBitmap(bitmap);
			this.imageViewImage.setPadding(3, 3, 3, 3);

			// show the dialog
			this.listingDetailDialog.setTitle(this.selectedListing.getTitle());
			this.listingDetailDialog.show();
		} catch (Exception x) {
			if ((this.listingDetailDialog != null)
					&& (this.listingDetailDialog.isShowing())) {
				this.listingDetailDialog.dismiss();
			}
			Log.e(TAG, "showListingDetailDialog", x);
		}
	}

	View.OnClickListener urlClickedListener = new View.OnClickListener() {
		public void onClick(View v) {
			launchBrowser();
		}
	};

	private void launchBrowser() {
		try {
			Intent browserIntent = new Intent(Intent.ACTION_VIEW,
					Uri.parse(this.selectedListing.getListingUrl()));
			this.startActivity(browserIntent);
		} catch (Exception x) {
			Log.e(TAG, "launchBrowser", x);
			this.showErrorDialog(x);
		}
	}

	android.content.DialogInterface.OnClickListener onListingDetailDialogCloseListener = new android.content.DialogInterface.OnClickListener() {
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
		}
	};

	private void invokeApi() {
		// get the clients from preference and then iterate

		// Thread thread = new Thread(new RequestThread("ebay", this.context,
		// handler));
		// thread.setName("ebay" + "thread");
		// this.threads.add(thread);
		// thread.start();

		Thread thread = new Thread(new RequestThread("google", this.context,
				handler));
		thread.setName("google" + "thread");
		this.threads.add(thread);
		thread.start();

		thread = new Thread(new RequestThread("invbox", this.context, handler));
		thread.setName("invbox" + "thread");
		this.threads.add(thread);
		thread.start();

		/*
		 * thread = new Thread(new RequestThread("amazon", this.context,
		 * handler)); thread.setName("amazon" + "thread");
		 * this.threads.add(thread); thread.start();
		 */
	}

	private synchronized TypedValue getValueForElementForClient(
			String elementName) {
		TypedValue outValue = new TypedValue();
		boolean resolveRefs = false;

		this.resources
				.getValue(this.resources.getIdentifier(elementName, "string",
						this.context.getPackageName()), outValue, resolveRefs);
		return outValue;
	}

	private synchronized String[] parseForTypesForClient(String typeString) {

		String delims = "[,]";
		String tmp;
		String[] tokens = typeString.split(delims);
		// creating a better order for search. the order would be UPC, ISBN,
		// ReferenceId
		String[] sorted = new String[tokens.length];
		for (int oCount = 0; oCount < tokens.length; oCount++) {
			switch (tokens[oCount].charAt(0)) {
			case 'U': {
				sorted[0] = tokens[oCount];
				break;
			}
			case 'I': {
				sorted[1] = tokens[oCount];
				break;
			}
			case 'R': {
				sorted[2] = tokens[oCount];
				break;
			}
			}
		}
		return sorted;
	}

	private TableLayout getView() throws MalformedURLException, IOException {
		return getViewForClients();
	}

	private TableLayout getViewForClients() throws MalformedURLException,
			IOException {

		/* Find Tablelayout defined in main.xml */
		TableLayout tl = (TableLayout) findViewById(R.id.listingdetaildialog_root);
		tl.setBackgroundColor(Color.BLACK);
		int i = 0;
		/* Create a new row to be added. */
		TableRow tr = null;

		for (int count = 0; count < allClients.size(); count++) {
			tr = new TableRow(this);
			// tr.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT,
			// LayoutParams.WRAP_CONTENT));
			TableLayout.LayoutParams tbll = new TableLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			TableRow.LayoutParams tblr = new TableRow.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

			tblr.setMargins(0, 10, 0, 0);
			// tr.setLayoutParams(tblr);
			// tr.setPadding(0, 10, 0, 0);
			tr.setBackgroundColor(Color.WHITE);

			TextView txtView = null;
			ImageView imgView = null;
			Iterator<Listing> iterator = allClients.get(count).getListings()
					.iterator();

			int itemCount = 0;
			while (iterator.hasNext()) {
				iterator.next();
				Listing listing = (Listing) allClients.get(count).getListings()
						.get(itemCount);
				if (itemCount == 0) {
					imgView = new ImageView(this.context);
					imgView.setMaxHeight(10);
					imgView.setMaxWidth(10);
					if ("ebay".equalsIgnoreCase(listing.getSiteName())) {
						imgView.setImageResource(R.drawable.ebay);
					} else if ("google".equalsIgnoreCase(listing.getSiteName())) {
						imgView.setImageResource(R.drawable.google);
					} else if ("invbox".equalsIgnoreCase(listing.getSiteName())) {
						imgView.setImageResource(R.drawable.invboxsmall);
					} else if ("amazon".equalsIgnoreCase(listing.getSiteName())) {
						imgView.setImageResource(R.drawable.amazon);
					}
					// imgView.setScaleType(ScaleType.CENTER_INSIDE);
					tr.addView(imgView, tblr);
				}

				// android.widget.LinearLayout.LayoutParams ll = new
				// LinearLayout.LayoutParams(150,150);
				LinearLayout ll = new LinearLayout(this.context);
				ll.setOrientation(LinearLayout.VERTICAL);
				ll.setGravity(Gravity.CENTER_HORIZONTAL);
				// set image content

				this.imageViewImage = new ImageView(this);
				// Log.w("url", ""+listing.getImageUrl());
				try {
					Bitmap bitmap = getImageFromURL(listing.getImageUrl());
					if (bitmap != null) {
						this.imageViewImage.setImageBitmap(bitmap);
					} else {
						this.imageViewImage
								.setImageResource(R.drawable.noimage);
					}
					this.imageViewImage.setPadding(3, 3, 3, 3);
					this.imageViewImage.setTag(listing.getSiteName() + "_"
							+ itemCount);
					this.imageViewImage.setAdjustViewBounds(true);
					this.imageViewImage.setMaxHeight(110);
					this.imageViewImage.setMaxWidth(110);
				} catch (Exception e) {
					Log.w("imageerror", e.getMessage());
				}
				// this.imageViewImage.set

				TextView price = new TextView(this.context);
				String priceStr = listing.getCurrentPrice();

				if ("invbox".equalsIgnoreCase(listing.getSiteName())) {
					String currency_symbol = getValueForElementForClient(
							"invbox_paypal_currency_symbol").coerceToString()
							.toString();
					priceStr = currency_symbol + priceStr;
				}

				if (itemCount == 0) {
					if (previousSearchPrice == null
							|| "".equals(previousSearchPrice)) {
						previousSearchPrice = priceStr;
					} else {
						float psp = Float.parseFloat(previousSearchPrice
								.substring(1));
						float curItemPrice = Float.parseFloat(priceStr
								.substring(1));
						if (psp > curItemPrice)
							previousSearchPrice = priceStr;
					}
				}

				price.setText(priceStr);
				price.setGravity(Gravity.CENTER_HORIZONTAL);

				/* Add Button to row. */
				this.imageViewImage.setOnClickListener(selectClickListener);

				registerForContextMenu(this.imageViewImage);
				this.imageViewImage
						.setOnLongClickListener(new View.OnLongClickListener() {

							public boolean onLongClick(View v) {
								v.showContextMenu();
								currentView = v;
								return true;
							}
						});
				// tr.setOnClickListener(selectClickListener);
				ll.addView(this.imageViewImage);
				ll.addView(price);
				tr.addView(ll, tblr);
				// tr.addView(price);

				/*
				 * // set image content Listing listing = (Listing)
				 * listings.getListings().get(0); this.imageViewImage =
				 * (ImageView) findViewById(R.id.listingdetail_image);
				 * 
				 * Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new
				 * URL( (listing).getImageUrl()).getContent());
				 * this.imageViewImage.setImageBitmap(bitmap);
				 * 
				 * // TextView titleField = (TextView)
				 * findViewById(R.id.listingdetail_auctionsource);
				 * titleField.setText(listing.getTitle() + "\nCurrent Price:" +
				 * listing.getCurrentPrice() + "\nLocation:" +
				 * listing.getLocation() + "\nShipping Cost:" +
				 * listing.getShippingCost()); TextView count = (TextView)
				 * findViewById(R.id.listingdetail_count); count.setText("eBay("
				 * + itemCount + ")"); // TextView // priceField
				 * =(TextView)layout.findViewById(R.id.listviewitem_price); //
				 * priceField
				 * .setText("Current price: "+listing.getCurrentPrice()); //
				 * TextView // shippingField=(TextView)layout.findViewById(R.id.
				 * listviewitem_shipping ); //
				 * shippingField.setText("Shipping: "
				 * +listing.getShippingCost());
				 */
				itemCount++;

			}
			/* Add row to TableLayout. */
			tl.addView(tr, new TableLayout.LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			i++;
		}

		return tl;
		// }
	}

	private void updateHistory(String content) {
		HistoryDataSource historyDS = new HistoryDataSource(this);
		historyDS.open();
		History history = historyDS.addToHistory(LoginActivity.userid, content);
		historyDS.close();
	}

	private Bitmap getImageFromURL(String url) {
		Bitmap bitmap = null;
		try {
			URLConnection urlConnection = new URL(url).openConnection();
			urlConnection.setConnectTimeout(30000);
			InputStream in = new BufferedInputStream(
					urlConnection.getInputStream());
			bitmap = BitmapFactory.decodeStream(in);

			// bitmap = BitmapFactory.decodeStream((InputStream) new URL(url)
			// .getContent());
			this.imageViewImage.setImageBitmap(bitmap);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.e("fetchingimage", e.getMessage() + "::" + url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("fetchingimage", e.getMessage() + "::" + url);
		}

		return bitmap;
	}

	private class RequestThread implements Runnable {

		private Context contextVal;
		private String clientName;
		private Handler handler;

		public RequestThread(String client, Context context, Handler handle) {
			this.contextVal = context;
			this.clientName = client;
			this.handler = handle;
		}

		public void run() {
			// TODO Auto-generated method stub
			callClientApi(this.clientName);
			totalThreads--;
			this.handler.sendEmptyMessage(RESULT_OK);
		}

		private synchronized void callClientApi(String client) {
			String searchResponse = "";
			try {
				invoker = new RequestInvoker(this.contextVal, client);
				ResponseParser parser = new ResponseParser(this.contextVal,
						client);

				SearchResult tempListings = new SearchResult();
				String[] types = parseForTypesForClient(getValueForElementForClient(
						client + "_search_types").coerceToString().toString());

				String site = getValueForElementForClient(client + "_site")
						.coerceToString().toString();

				String reqType = getValueForElementForClient(
						client + "_request_type").coerceToString().toString();

				boolean expectionOccured = true;
				Pattern p = Pattern.compile("[a-zA-Z]");
				updateHistory(searchTerm);
				searchResponse = invoker.search(URLEncoder.encode(searchTerm),
						site, reqType, "keyword", client);

				try {

					tempListings.setListings(parser.parseListings(
							searchResponse, reqType, client));
					allClients.add(tempListings);
					if (tempListings != null && tempListings.getListings() != null
							&& tempListings.getListings().get(0) != null
							&& (itemUPCCode == null||"".equals(itemUPCCode))){
						// updateHistory(listings.getListings().get(0)
						// .getCurrentPrice());
						itemUPCCode = tempListings.getListings().get(0).getId();
						Log.w("itemUPCCode",""+client+"::"+itemUPCCode);
					}
				} catch (Exception e) {
					// do nothing until all types fnhas been searched for
					// then do what?
					expectionOccured = true;
					e.printStackTrace();
					// throw e;
				}
				/*
				 * } else { out: for (int count = 0; count < types.length;
				 * count++) { expectionOccured = false; searchResponse =
				 * ebayInvoke.search(searchTerm, types[count], ebaySite,
				 * reqType, "product");
				 * 
				 * try { listings.setListings(ebayParser.parseListings(
				 * searchResponse, reqType)); // getView(); break out; } catch
				 * (Exception e) { // do nothing until all types has been
				 * searched for // then do what? expectionOccured = true;
				 * e.printStackTrace(); } } }
				 */
				// this.handler.sendEmptyMessage(RESULT_OK);
			} catch (Exception x) {
				Log.e(TAG, "LoadListThread.run(): searchResponse="
						+ searchResponse, x);
				// listings.setError(x);
				if ((progressDialog != null) && (progressDialog.isShowing())) {
					progressDialog.dismiss();
				}
				// showErrorDialog(x);
			}
		}
	}
}