package com.ecinv.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.ecinv.db.bo.History;
import com.ecinv.db.datasource.HistoryDataSource;
import com.ecinv.db.datasource.ItemDataSource;
import com.ecinv.db.datasource.UserDataSource;
import com.ecinv.scanner.IntentIntegrator;
import com.ecinv.scanner.IntentResult;
import com.ecinv.util.Util;
import com.invbox.R;

public class MyBarCodeScannerActivity extends Activity {

	private UserDataSource userDS;
	private HistoryDataSource historyDS;
	private ItemDataSource itemDS;
	private EditText edit;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		userDS = new UserDataSource(this);
		historyDS = new HistoryDataSource(this);
		// itemDS = new ItemDataSource(this);

		userDS.open();
		historyDS.open();
		// itemDS.open();

		setContentView(R.layout.main);
		edit = (EditText) findViewById(R.id.codeText);

		// for testing
		edit.setText("786936284065");
	

		final IntentIntegrator integrator = new IntentIntegrator(this);
		final Button scanBtn = (Button) findViewById(R.id.scanBtn);
		// integrator.initiateScan();
		final Button searchBtn = (Button) findViewById(R.id.searchBtn);

		scanBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				integrator.initiateScan();
			}
		});

		searchBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				String searchTerm = edit.getText().toString();
				updateHistory(searchTerm);
				Intent tableLayout = new Intent(v.getContext(),
						TableLayoutActivity.class);
				tableLayout.putExtra("searchTerm", searchTerm);
				startActivity(tableLayout);

			}
		});
	} // end of onCreate

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		IntentResult result = IntentIntegrator.parseActivityResult(requestCode,
				resultCode, intent);
		if (result != null) {
			String contents = result.getContents();
			if (contents != null) {
				EditText edit = (EditText) findViewById(R.id.codeText);
				edit.setText(contents + "::" + result.getFormatName());
				URL url;
				// setContentView(R.layout.listviewitem);

				/*
				 * InputStream iostream = null; try { url = new URL(
				 * "http://www.upcdatabase.com/bookland.asp?upc=9781405322355");
				 * iostream = url.openConnection().getInputStream(); String data
				 * = getStreamContents(iostream);
				 * 
				 * edit.setText(data); // data.substring( data.indexOf( //
				 * "yui-dt0-bodytable" ) ) ); Log.w("Data", data.substring(data
				 * .indexOf("id=\"contentwrapper\""))); } catch
				 * (MalformedURLException e) { // TODO Auto-generated catch
				 * block // e.printStackTrace(); } catch (IOException e) { //
				 * TODO Auto-generated catch block // e.printStackTrace(); }
				 */
				// showDialog( R.string.hello, result.toString());
			} else {
				// showDialog(R.string.hello);
			}
		}
	}

	private String getStreamContents(InputStream stream) throws IOException {

		StringBuffer content = new StringBuffer(1024);
		String line;
		BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(stream));
		//
		while ((line = bufferedReader.readLine()) != null) {
			content.append(line);
			// Log.w("getStreamContents",line);
		}

		bufferedReader.close();
		return content.toString();
	}

	private void updateHistory(String content) {
		try {
			History history = historyDS.addToHistory(LoginActivity.userid, content);
			updateInvboxHistory(content);
		} catch (Exception e) {
			// a user might exist
		}
	}

	private synchronized String updateInvboxHistory(String keyword)
			throws Exception {

		String result = null;
		InputStream is = null;
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("itemcode", keyword));
		nameValuePairs
				.add(new BasicNameValuePair("userprofile.id", "1"));
		nameValuePairs.add(new BasicNameValuePair("listprice", "100.0"));	

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(
					"http://"+Util.SERVER_IP+"/invbox/history/save");
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
			HttpResponse response = httpclient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			byte[] b = new byte[1024];
			while( is.available() > 0 ){
				is.read(b);
				Log.w("saveerror", new String(b));
			}
			
			Log.d("HTTP", "HTTP: OK");
		} catch (Exception e) {
			Log.e("HTTP", "Error in http connection " + e.toString());
		}
		return (result);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		historyDS.close();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		userDS.open();
		historyDS.open();
		// itemDS.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		userDS.close();
		historyDS.close();
		// itemDS.open();
		super.onPause();
	}

} // end of class MyBarCodeScannerActivity