package com.ecinv.activity;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.ecinv.db.bo.Item;
import com.ecinv.db.datasource.ItemDataSource;
import com.invbox.R;
import com.paypal.android.MEP.CheckoutButton;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalPayment;

public class InvboxPaypalActivity extends Activity implements OnClickListener {

	private Resources resources;
	private Context context;
	private ItemDataSource itemDS;
	private String price;
	private String serverId;
	private String status;
	private String id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.paypal);

		this.resources = getResources();
		this.context = this;

		CheckoutButton launchPayPalButton = TableLayoutActivity
				.getPaypalInstance().getCheckoutButton(this,
						PayPal.BUTTON_152x33, CheckoutButton.TEXT_PAY);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		params.addRule(RelativeLayout.ALIGN_PARENT_TOP);

		params.topMargin = 10;

		launchPayPalButton.setLayoutParams(params);
		launchPayPalButton.setOnClickListener(this);

		((RelativeLayout) findViewById(R.id.relativeLayout1))
				.addView(launchPayPalButton);
		launchPayPalButton.performClick();
		finish();
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub

		PayPalPayment newPayment = new PayPalPayment();
		Bundle extras = getIntent().getExtras();
		String recepient = "";
		if (extras != null) {
			price = extras.getString("price").trim();
			serverId = extras.getString("serverId");
			id = extras.getString("id");
			recepient = extras.getString("recepient");
		}
		try {
			String currency_symbol = getValueForElementForClient(
					"invbox_paypal_currency_symbol").coerceToString()
					.toString();
			price = price.replace(currency_symbol, "");
			NumberFormat.getInstance().parse(price);
		} catch (ParseException e) {
			// Not a number.
			price = "";
		}
		newPayment.setSubtotal(new BigDecimal((((price == null || ""
				.equals(price))) ? "0" : ""+price)));
		newPayment.setCurrencyType("USD");
		recepient = recepient.replace("_dot_", ".");
		newPayment.setRecipient(recepient);
		newPayment.setMerchantName("inVbox");
		
		Intent paypalIntent = PayPal.getInstance().checkout(newPayment, this);
//		paypalIntent.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		this.startActivityForResult(paypalIntent, 1);

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (resultCode) {
		case Activity.RESULT_OK:
			// The payment succeeded
			try {

				String payKey = data
						.getStringExtra(PayPalActivity.EXTRA_PAY_KEY);
				Toast.makeText(this.getApplicationContext(),
						"Payment was successful", Toast.LENGTH_SHORT).show();
				// Should set the item as bought and move out of the market.
				itemDS = new ItemDataSource(this);
				itemDS.open();

				Item item = itemDS.getItem(id);
				item.setStatus("bought");
				item.setPurchasePrice(Double.parseDouble(price));

				itemDS.updateItemStatus(item);
			} catch (Exception e) {
				Log.w("paypalresult", e.toString());
			} finally {
			itemDS.close();
			}
			// Tell the user their payment succeeded
			break;
		case Activity.RESULT_CANCELED:
			// The payment was canceled
			// Tell the user their payment was canceled
			break;
		case PayPalActivity.RESULT_FAILURE:
			// The payment failed -- we get the error from the EXTRA_ERROR_ID
			// and EXTRA_ERROR_MESSAGE
			String errorID = data.getStringExtra(PayPalActivity.EXTRA_ERROR_ID);
			String errorMessage = data
					.getStringExtra(PayPalActivity.EXTRA_ERROR_MESSAGE);
			Toast.makeText(this.getApplicationContext(), errorMessage,
					Toast.LENGTH_SHORT).show();
			// Tell the user their payment was failed.
		}
	}

	private synchronized TypedValue getValueForElementForClient(
			String elementName) {
		TypedValue outValue = new TypedValue();
		boolean resolveRefs = false;

		this.resources
				.getValue(this.resources.getIdentifier(elementName, "string",
						this.context.getPackageName()), outValue, resolveRefs);
		return outValue;
	}

}
