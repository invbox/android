package com.ecinv.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.ecinv.db.bo.Item;
import com.ecinv.db.datasource.CategoryDataSource;
import com.ecinv.db.datasource.ItemDataSource;
import com.invbox.R;

public class InventoryActivity extends Activity {
	// ListActivity {

	private ItemDataSource itemDS;
	private CategoryDataSource categoryDS;
	private List<Item> items;
	private Context context;
	private ImageView addImageView;
	private ImageView editImageView;
	private ImageView deleteImageView;
	private ImageView marketImageView;
	public static final int PlaceInMarket = Menu.FIRST;

	private ListView mainListView;
	private ArrayAdapter<Item> listAdapter;

	public void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.inventorymain);

			mainListView = (ListView) findViewById(R.id.mainListView);

			addImageView = (ImageView) findViewById(R.id.add_image);
			addImageView.setTag(new Integer("1"));

			editImageView = (ImageView) findViewById(R.id.edit_image);
			editImageView.setTag(new Integer("2"));

			deleteImageView = (ImageView) findViewById(R.id.delete_image);
			deleteImageView.setTag(new Integer("3"));

			marketImageView = (ImageView) findViewById(R.id.market_image);
			marketImageView.setTag(new Integer("4"));

			this.addImageView.setOnClickListener(selectClickListener);
			this.editImageView.setOnClickListener(selectClickListener);
			this.deleteImageView.setOnClickListener(selectClickListener);
			this.marketImageView.setOnClickListener(selectClickListener);

			mainListView
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {

						public void onItemClick(AdapterView<?> parent,
								View view, int position, long id) {
							// TODO Auto-generated method stub
							Item item = listAdapter.getItem(position);
							item.toggleChecked();
							ItemViewHolder viewHolder = (ItemViewHolder) view
									.getTag();
							viewHolder.getCheckBox().setChecked(
									item.isChecked());
						}
					});
			setItemsForView();

		} catch (Exception x) {
			// Log.e(TagHandler, "onCreate", x);
			// this.showErrorDialog(x);
		}
	}

	private void removeView(List<Item> list) {
		for (int count = 0; count < list.size(); count++) {
			listAdapter.remove(list.get(count));
		}
	}

	private void setItemsForView() {
		if (itemDS == null) {
			itemDS = new ItemDataSource(this);
		}
		if (categoryDS == null) {
			categoryDS = new CategoryDataSource(this);
		}
		categoryDS.open();
		itemDS.open();
		items = itemDS.getItemsForTypesAndStatuses(new String[] { "sell" },
				null,categoryDS);
		
		

		listAdapter = new ItemArrayAdapter(this, items);
		mainListView.setAdapter(listAdapter);
		// refreshView(items);
	}

	OnClickListener selectClickListener = new OnClickListener() {

		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (((Integer) v.getTag()).intValue()) {

			case 1: {// add

				Intent inventoryIntent = new Intent(v.getContext(),
						NewInventoryActivity.class);
				inventoryIntent.putExtra("edit", "false");
				startActivity(inventoryIntent);
				break;
			}
			case 2: {// edit
				ArrayList<Item> tItems = new ArrayList<Item>();
				int multiSelect = 0;
				for (int count = 0; count < listAdapter.getCount(); count++) {
					Item item = listAdapter.getItem(count);
					if (item.isChecked()) {
						tItems.add(item);
						multiSelect++;
						if (multiSelect > 1)
							break;
					}
				}
				if (tItems.size() <= 0)
					break;
				if (tItems.size() > 1) {
					new AlertDialog.Builder(InventoryActivity.this)
							.setTitle("Edit Error")
							.setMessage("Select only one item for editing.")
							.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int whichButton) {
										}
									}).show();
				} else {
					Intent inventoryIntent = new Intent(v.getContext(),
							NewInventoryActivity.class);
					Item item = (Item) tItems.get(0);
					inventoryIntent.putExtra("edit", "true");
					inventoryIntent.putExtra("itemName", item.getItemname());
					inventoryIntent.putExtra("itemDesc", item.getDescription());
					inventoryIntent.putExtra("itemCode", item.getItemCode());
					inventoryIntent.putExtra("status", item.getStatus());
					inventoryIntent.putExtra("type", item.getType());
					inventoryIntent.putExtra("serverid", item.getServerId());
					inventoryIntent.putExtra("id", "" + item.getId());
					inventoryIntent.putExtra("purchasePrice",
							"" + item.getPurchasePrice());
					inventoryIntent.putExtra("purchaseDate",
							"" + item.getPurchaseDate());
					inventoryIntent.putExtra("categoryCode",
							"" + item.getCategoryCode());
					startActivity(inventoryIntent);
				}
				break;
			}
			case 3: {// delete

				ArrayList<Item> tItems = new ArrayList<Item>();
				for (int count = 0; count < listAdapter.getCount(); count++) {
					Item item = listAdapter.getItem(count);
					if (item.isChecked()) {
						tItems.add(item);
					}
				}
				if (tItems.size() <= 0)
					break;

				new AlertDialog.Builder(InventoryActivity.this)
						.setTitle("Delete")
						.setMessage("Are you sure to delete the selected item?")
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										// need to change shud use the above
										// iteration...:-(
										ArrayList<Item> tItems = new ArrayList<Item>();
										for (int count = 0; count < listAdapter
												.getCount(); count++) {
											Item item = listAdapter
													.getItem(count);
											if (item.isChecked()) {
												tItems.add(item);
											}
										}
										itemDS.deleteItems(tItems);
										removeView(tItems);
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								}).show();

				// setItemsForView();
				break;
			}
			case 4: {// update market place

				ArrayList<Item> tItems = new ArrayList<Item>();

				for (int count = 0; count < listAdapter.getCount(); count++) {
					Item item = listAdapter.getItem(count);
					if (item.isChecked()) {
						tItems.add(item);
					}
				}
				if (tItems.size() <= 0) {
					new AlertDialog.Builder(InventoryActivity.this)
							.setTitle("Pushing to Market Place")
							.setMessage(
									"No item(s) selected to place in the market. Please select item(s) and then click on the Market button above.")
							.setPositiveButton("Ok",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int whichButton) {
										}
									}).show();
					break;
				}

				new AlertDialog.Builder(InventoryActivity.this)
						.setTitle("Pushing to Market Place")
						.setMessage(
								"The selected item(s) would be placed in market place.")
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int whichButton) {
										// need to change shud use the above
										// iteration...:-(
										ArrayList<Item> tItems = new ArrayList<Item>();
										for (int count = 0; count < listAdapter
												.getCount(); count++) {
											Item item = listAdapter
													.getItem(count);
											if (item.isChecked()) {
												tItems.add(item);
											}
										}
										ContentValues values = new ContentValues();
										values.put("status", "market");
										itemDS.updateItems(tItems, values);
										setItemsForView();
										// refreshView(tItems);
									}
								})
						.setNegativeButton("No",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								}).show();

				// setItemsForView();
				break;
			}
			}
		}
	};

	private boolean isFieldValid() {
		return true;
	}

	private class ItemArrayAdapter extends ArrayAdapter<Item> {

		private LayoutInflater inflater;

		public ItemArrayAdapter(Context context, List<Item> itemList) {
			super(context, R.layout.inventory, R.id.rowTextView, itemList);
			inflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			Item item = (Item) this.getItem(position);

			// The child views in each row.
			CheckBox checkBox;
			TextView textView;

			// Create a new row view
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.inventory, null);

				// Find the child views.
				textView = (TextView) convertView
						.findViewById(R.id.rowTextView);

				if ("inventory".equalsIgnoreCase(item.getStatus())) {
					// blue
					textView.setBackgroundColor(Color.parseColor("#4387F5"));
				} else if ("market".equalsIgnoreCase(item.getStatus())) {
					// amber
					textView.setBackgroundColor(Color.parseColor("#FFDF7C"));
				} else if ("sold".equalsIgnoreCase(item.getStatus())) {
					// green
					textView.setBackgroundColor(Color.parseColor("#00A513"));
				}

				checkBox = (CheckBox) convertView.findViewById(R.id.CheckBox01);

				// Optimization: Tag the row with it's child views, so we don't
				// have to
				// call findViewById() later when we reuse the row.
				convertView.setTag(new ItemViewHolder(textView, checkBox));

				// If CheckBox is toggled, update the item it is tagged with.
				checkBox.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						CheckBox cb = (CheckBox) v;
						Item item = (Item) cb.getTag();
						item.setChecked(cb.isChecked());
					}
				});
			}
			// Reuse existing row view
			else {
				// Because we use a ViewHolder, we avoid having to call
				// findViewById().
				ItemViewHolder viewHolder = (ItemViewHolder) convertView
						.getTag();
				checkBox = viewHolder.getCheckBox();
				textView = viewHolder.getTextView();
			}

			// Tag the CheckBox with the Planet it is displaying, so that we can
			// access the planet in onClick() when the CheckBox is toggled.
			checkBox.setTag(item);

			// Display planet data
			checkBox.setChecked(item.isChecked());
			textView.setText(item.getItemname());

			return convertView;
		}

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (itemDS == null) {
			itemDS = new ItemDataSource(this);
		}
		itemDS.open();
		setItemsForView();
		Log.w("onresume", "onresume");
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		itemDS.close();
		super.onPause();
	}

	/** Holds child views for one row. */
	private static class ItemViewHolder {
		private CheckBox checkBox;
		private TextView textView;

		public ItemViewHolder() {
		}

		public ItemViewHolder(TextView textView, CheckBox checkBox) {
			this.checkBox = checkBox;
			this.textView = textView;
		}

		public CheckBox getCheckBox() {
			return checkBox;
		}

		public void setCheckBox(CheckBox checkBox) {
			this.checkBox = checkBox;
		}

		public TextView getTextView() {
			return textView;
		}

		public void setTextView(TextView textView) {
			this.textView = textView;
		}
	}
}
