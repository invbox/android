package com.ecinv.activity;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.ecinv.db.bo.User;
import com.ecinv.db.datasource.UserDataSource;
import com.ecinv.util.Base64;
import com.ecinv.util.CryptographyUtil;
import com.invbox.R;

public class CreateNewUserActivity extends Activity {

	private UserDataSource userDS;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_user);
		userDS = new UserDataSource(this);
		userDS.open();

		Button createBtn = (Button) findViewById(R.id.createUserBtn);
		createBtn.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				EditText tempEditText = (EditText) findViewById(R.id.newuser_userIdEditText);
				String userid = null, firstName = null, lastName = null, password = null;
				if (tempEditText.getText() != null
						&& !"".equals(tempEditText.getText())) {
					userid = tempEditText.getText().toString();
				}

				tempEditText = (EditText) findViewById(R.id.newuser_firstNameEditText);
				if (tempEditText.getText() != null
						&& !"".equals(tempEditText.getText())) {
					firstName = tempEditText.getText().toString();
				}

				tempEditText = (EditText) findViewById(R.id.newuser_lastnamEditText);
				if (tempEditText.getText() != null
						&& !"".equals(tempEditText.getText())) {
					lastName = tempEditText.getText().toString();
				}

				tempEditText = (EditText) findViewById(R.id.newuser_passwordEditText);
				if (tempEditText.getText() != null
						&& !"".equals(tempEditText.getText())) {
					password = tempEditText.getText().toString();
				}

				// Sending side
				String data = null;
				try {
					data = CryptographyUtil.encrypt(password);
//							encryptPassword(password).getBytes();
//					User user = userDS.createUser(userid, firstName, lastName,
//							"", Base64.encodeBytes(password.getBytes()));
					User user = userDS.createUser(userid, firstName, lastName,
							"", data);
					
//					decryptPassword(Base64.decode(user.getPassword()));
					finish();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					Log.w("e2", e.getMessage());
				} finally {
					userDS.close();
				}
			}
		});

	}
	
	private boolean checkUserExists(){
		 return false;
	}

	private String encryptPassword(String password) {
		byte[] b = password.getBytes();
		// write getkey from server, if present get it otherwise create a new one and get it.
		
//		byte[] keyStart = "invbox2012".getBytes();
//		KeyGenerator kgen = null;
//		SecureRandom sr = null;
//		SecretKey skey = null;
//		byte[] key = null;
		byte[] encryptedData = null;
		byte[] decryptedData = null;
		try {
//			kgen = KeyGenerator.getInstance("AES");
//			sr = SecureRandom.getInstance("SHA1PRNG");
//			sr.setSeed(keyStart);
//			kgen.init(128, sr); // 192 and 256 bits may not be available
//			skey = kgen.generateKey();
//			key = skey.getEncoded();
			encryptedData = encrypt(Base64.decode(new String(LoginActivity.pKey)), b);
			Log.w("encryptedPWD", new String(encryptedData));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String(encryptedData);
	}

	private String decryptPassword(byte[] b) {
		/*byte[] keyStart = "invbox2012".getBytes();
		KeyGenerator kgen = null;
		SecureRandom sr = null;
		SecretKey skey = null;
		byte[] key = null;
		byte[] encryptedData = null;*/
		byte[] decryptedData = null;
		try {
			/*kgen = KeyGenerator.getInstance("AES");
			sr = SecureRandom.getInstance("SHA1PRNG");
			sr.setSeed(keyStart);
			kgen.init(128, sr); // 192 and 256 bits may not be available
			skey = kgen.generateKey();
			key = skey.getEncoded();*/
			decryptedData = decrypt(Base64.decode(new String(LoginActivity.pKey)), b);
			Log.w("decryptedPWD", new String(decryptedData));

		} catch (Exception e) {
			Log.w("1234", e.getMessage());
		}
		return new String(decryptedData);
	}

	private static byte[] encrypt(byte[] raw, byte[] clear) throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
		byte[] encrypted = cipher.doFinal(clear);
		return encrypted;
	}

	private static byte[] decrypt(byte[] raw, byte[] encrypted)
			throws Exception {
		SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES/CBC/PKCS5Padding");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, skeySpec);
		byte[] decrypted = cipher.doFinal(encrypted);
		return decrypted;
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		if (userDS == null) {
			userDS = new UserDataSource(this);
		}
		userDS.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		userDS.close();
		super.onPause();
	}
}