package com.ecinv.util;

public class InventoryListing {
	
	private int id;
	private int itemcode;
	private String itemname;
	private String itemdescription;
	private double purchaseprice;
	private String image;
	private int categorycode;
	private String createdby;
	private double creationdate;
	private String lastmodifiedby;
	private double lastmodifieddate;
	private int profilecode;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getItemcode() {
		return itemcode;
	}
	public void setItemcode(int itemcode) {
		this.itemcode = itemcode;
	}
	public String getItemname() {
		return itemname;
	}
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	public String getItemdescription() {
		return itemdescription;
	}
	public void setItemdescription(String itemdescription) {
		this.itemdescription = itemdescription;
	}
	public double getPurchaseprice() {
		return purchaseprice;
	}
	public void setPurchaseprice(double purchaseprice) {
		this.purchaseprice = purchaseprice;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getCategorycode() {
		return categorycode;
	}
	public void setCategorycode(int categorycode) {
		this.categorycode = categorycode;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public double getCreationdate() {
		return creationdate;
	}
	public void setCreationdate(double creationdate) {
		this.creationdate = creationdate;
	}
	public String getLastmodifiedby() {
		return lastmodifiedby;
	}
	public void setLastmodifiedby(String lastmodifiedby) {
		this.lastmodifiedby = lastmodifiedby;
	}
	public double getLastmodifieddate() {
		return lastmodifieddate;
	}
	public void setLastmodifieddate(double lastmodifieddate) {
		this.lastmodifieddate = lastmodifieddate;
	}
	public int getProfilecode() {
		return profilecode;
	}
	public void setProfilecode(int profilecode) {
		this.profilecode = profilecode;
	}
	

}
