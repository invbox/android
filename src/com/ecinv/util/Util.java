package com.ecinv.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ImageView;

import com.ecinv.db.datasource.HistoryDataSource;
import com.ecinv.db.datasource.ItemDataSource;
import com.ecinv.db.datasource.UserDataSource;

public class Util {

	private static UserDataSource userDS;

	private static HistoryDataSource historyDS;
	private static ItemDataSource itemDS;

	public static String SERVER_IP =
	// "192.168.3.94:8080";
	"192.168.1.83:8080";

	public static void createDatasources(Activity activity) {
		userDS = new UserDataSource(activity);
		historyDS = new HistoryDataSource(activity);
	}

	public static UserDataSource getUserDS() {
		return userDS;
	}

	public static void setUserDS(UserDataSource userDS) {
		Util.userDS = userDS;
	}

	public static HistoryDataSource getHistoryDS() {
		return historyDS;
	}

	public static void setHistoryDS(HistoryDataSource historyDS) {
		Util.historyDS = historyDS;
	}

	public static ItemDataSource getItemDS() {
		return itemDS;
	}

	public static void setItemDS(ItemDataSource itemDS) {
		Util.itemDS = itemDS;
	}

	public static Bitmap getImageFromURL(String url, ImageView imageView) {
		Bitmap bitmap = null;
		try {
			bitmap = BitmapFactory.decodeStream((InputStream) new URL(url)
					.getContent());
			imageView.setImageBitmap(bitmap);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return bitmap;
	}

	public static String getProductDetails(Resources resources,
			Context context, String keyword) throws Exception {

		String key = getValueForElementForClient("product_key_production",
				resources, context).coerceToString().toString();
		String upcUrl = getValueForElementForClient("product_wsurl_production",
				resources, context).coerceToString().toString();
		String template = getValueForElementForClient(
				"product_details_request_template_for_product", resources,
				context).coerceToString().toString();

		String response = invokeUPCService(keyword, template, upcUrl, key);
		// Log.w("upc", response);
		// response =
		// "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><output xmlns=\"http://www.upcdatabase.org/\"><valid>true</valid><number>0111222333444</number><itemname>UPC Database Testing Code</itemname><description>http://www.upcdatabase.org/code/0111222333444</description><price>123.45</price><ratingsup>2</ratingsup><ratingsdown>0</ratingsdown></output>";

		return getProductName(response, resources, context);
	}

	private static String getProductName(String response, Resources resources,
			Context context) {
		Document doc = xmlFromString(response);
		String productName = "";

		NodeList nodelist = doc
				.getElementsByTagName(getValueForElementForClient(
						"product_itemname", resources, context)
						.coerceToString().toString());

		if (nodelist != null && nodelist.getLength() > 0)
			productName = getElementValue((Node) nodelist.item(0));

		return productName;
	}

	public static Document xmlFromString(String xml) {

		Document doc = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			doc = db.parse(is);
		} catch (ParserConfigurationException e) {
			System.out.println("XML parse error: " + e.getMessage());
			return null;
		} catch (SAXException e) {
			System.out.println("Wrong XML file structure: " + e.getMessage());
			return null;
		} catch (IOException e) {
			System.out.println("I/O exeption: " + e.getMessage());
			return null;
		}
		return doc;
	}

	private static TypedValue getValueForElementForClient(String elementName,
			Resources resources, Context context) {
		TypedValue outValue = new TypedValue();
		boolean resolveRefs = false;

		resources.getValue(
				resources.getIdentifier(elementName, "string",
						context.getPackageName()), outValue, resolveRefs);
		return outValue;
	}

	private static synchronized String invokeUPCService(String keyword,
			String template, String url, String key) throws Exception {
		String result = null;
		HttpClient httpClient = new DefaultHttpClient();
		Log.w("upcurl", getRequestURL(keyword, template, url, key));
		HttpGet httpGet = new HttpGet(
				getRequestURL(keyword, template, url, key));
		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity httpEntity = response.getEntity();
		if (httpEntity != null) {
			InputStream in = httpEntity.getContent();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuffer temp = new StringBuffer();
			String currentLine = null;
			while ((currentLine = reader.readLine()) != null) {
				temp.append(currentLine);
			}
			result = temp.toString();
			in.close();
		}
		return (result);
	}

	private static synchronized String getRequestURL(String keyword,
			String template, String url, String appID) {
		CharSequence requestURL = TextUtils.expandTemplate(template, url,
				appID, keyword);
		return (requestURL.toString());
	}

	public final static String getElementValue(Node elem) {
		Node kid;
		if (elem != null) {
			if (elem.hasChildNodes()) {
				for (kid = elem.getFirstChild(); kid != null; kid = kid
						.getNextSibling()) {
					if (kid.getNodeType() == Node.TEXT_NODE) {
						return kid.getNodeValue();
					}
				}
			}
		}
		return "";
	}

	public synchronized static Date convertStringToDate(String dateStr) {
		if (dateStr == null || "".equals(dateStr))
			return null;
		SimpleDateFormat purSDFDate = new SimpleDateFormat(
				"yyyy-MM-dd hh:mm:ss");
		Date purDate = null;
		try {
			purDate = purSDFDate.parse(dateStr.replace("T", " "));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Log.w("dateformatexception",
					"dateStr::" + dateStr + "::" + e.getMessage());
			return null;
		}
		return purDate;
	}

}