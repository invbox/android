/*
ebayDemo - Demo of how to call the eBay API and display the results on an Android device
Imported from TurboGrafx16Collector - Mobile application to manage a collection of TurboGrafx-16 games
Copyright (C) 2010 Hugues Johnson

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software 
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package com.ecinv.util;

import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class InventoryArrayAdapter extends ArrayAdapter<String>{
	private final static String TAG="InventoryArrayAdapter";
	private Context context;
	private List listings;
	private int resourceId;
	private Drawable images[];
	
	public InventoryArrayAdapter(Context context,int resourceId, List<String> list){
//			SearchResult listings){
		super(context,resourceId,list);
//		this.images=new Drawable[listings.getListings().size()];
		this.context=context;
		this.listings=list;
		this.resourceId=resourceId;
		Log.w("invadapter", "invadapter");
	}

	@Override
	public View getView(int position,View convertView,ViewGroup parent){
		Log.w("getview", "getview");
		LayoutInflater inflater=(LayoutInflater)this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout=inflater.inflate(resourceId,null);
		String listing= (String) this.listings.get(position);
//		TextView titleField=(TextView)layout.findViewById(R.id.inventory_item);
//		titleField.setText(listing);
//		TextView priceField=(TextView)layout.findViewById(R.id.listviewitem_price);
//		priceField.setText("Current price: "+listing.getCurrentPrice());
//		TextView shippingField=(TextView)layout.findViewById(R.id.listviewitem_shipping);
//		shippingField.setText("Shipping: "+listing.getShippingCost());
		/*if(listing.getListingUrl()!=null){
			try{
				if( listing.getImageUrl() == null )
					return layout;
				URL url=new URL(listing.getImageUrl());
				InputStream is=(InputStream)url.getContent();
				Drawable image=Drawable.createFromStream(is,"src");
				ImageView imageView=new ImageView(context);
//				imageView=(ImageView)layout.findViewById(R.id.listviewitem_image);
				imageView.setImageDrawable(image);
				this.images[position]=image;
			}catch(Exception x){
				Log.e(TAG,"getView - in if(listing.getListingUrl()!=null)",x);
			}
		}*/
		return(layout);  	
	}
	
	public Drawable getImage(int position){
		return(this.images[position]);
	}
}