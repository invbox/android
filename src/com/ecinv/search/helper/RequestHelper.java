package com.ecinv.search.helper;

import android.content.Context;
import android.content.res.Resources;

public interface RequestHelper {

	public String prepareRequestString(String keyword, String ebaySite,
			String reqType, String templateId, String url, String appID, String client);
	public void setResources(Resources resources);
	public void setContext(Context context);
}
