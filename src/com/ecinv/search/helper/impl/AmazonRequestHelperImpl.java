package com.ecinv.search.helper.impl;

import java.io.StringReader;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.util.TypedValue;

import com.ecinv.search.helper.AmazonSignedRequestsHelper;
import com.ecinv.search.helper.RequestHelper;

public class AmazonRequestHelperImpl implements RequestHelper {

	private Resources resources = null;
	private Context context = null;

	public void setResources(Resources resources) {
		this.resources = resources;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	private String getVersionString() {
		Calendar cal = Calendar.getInstance();
		return "" + cal.get(Calendar.YEAR) + "-"
				+ (cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.DATE);
	}

	public String prepareRequestString(String keyword, String ebaySite,
			String reqType, String templateId, String url, String appID,
			String client) {
		String searchUrl = getSearchUrl(keyword, ebaySite, reqType, templateId,
				url, appID, client);
		return searchUrl;
	}

	private synchronized TypedValue getValueForElementForClient(
			String elementName) {
		TypedValue outValue = new TypedValue();
		boolean resolveRefs = false;

		this.resources
				.getValue(this.resources.getIdentifier(elementName, "string",
						this.context.getPackageName()), outValue, resolveRefs);
		return outValue;
	}

	public String getValueFromContents(String contents, String searchElementName) {
		String productGroup = "";
		try {
			if (contents != null && !"".equals(contents)) {

				Document doc = xmlFromString(contents);

				NodeList nodeList = doc.getElementsByTagName(searchElementName);
				if (nodeList == null || nodeList.getLength() <= 0) {
					throw new Exception(
							"Seems to a problem while contacting the service.");
				}
				Node node = nodeList.item(0);
				productGroup = getElementValue(node);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productGroup;
	}

	public final static String getElementValue(Node elem) {
		Node kid = null;
		try {
			if (elem != null) {
				if (elem.hasChildNodes()) {
					for (kid = elem.getFirstChild(); kid != null; kid = kid
							.getNextSibling()) {
						if (kid.getNodeType() == Node.TEXT_NODE) {
							return kid.getNodeValue();
						}
					}
				}
			}
		} catch (Exception e) {
			Log.w("getElementvalue::", kid.getLocalName());
			Log.w("getElementvalueEx::", e.getMessage());
		}
		return "";
	}

	public synchronized Document xmlFromString(String xml) {

		Document doc = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Log.w("xmlresponse", xml);

		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			doc = db.parse(is);
		} catch (Exception e) {
			Log.e("transforming", e.getMessage());
		}

		return doc;
	}

	private String getSearchUrl(String keyword, String ebaySite,
			String reqType, String templateId, String url, String appID,
			String client) {

		String awsAccessKey = getValueForElementForClient(
				client + "_appid_production").coerceToString().toString();
		String awsSecretKey = getValueForElementForClient(
				client + "_awssecretkey1_production").coerceToString()
				.toString();
		String awsEndPoint = getValueForElementForClient(
				client + "_wsurl_production").coerceToString().toString();
		int index = 0;
		String protocol = "http://";
		String endPoint = "";
		if ((index = awsEndPoint.toLowerCase().indexOf("http://") + 7) > 6
				|| (index = awsEndPoint.toLowerCase().indexOf("https://") + 8) > 7) {
			protocol = awsEndPoint.substring(0, index);
			endPoint = awsEndPoint.substring(index);
		}
		AmazonSignedRequestsHelper helper;
		try {
			helper = AmazonSignedRequestsHelper.getInstance(endPoint,
					awsAccessKey, awsSecretKey, protocol);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

		String requestUrl = null;
		String title = null;

		if (keyword != null && keyword.length() < 13) {
			for (int count = 13; count > keyword.length(); count--) {
				keyword = "0" + keyword;
			}
		}
		String associateId = getValueForElementForClient(
				client + "_associateid_production").coerceToString().toString();
		Map<String, String> params = createRequestMapForSearch(keyword,
				associateId);
		requestUrl = helper.sign(params);
		Log.w("awsurl", requestUrl);

		return requestUrl;

	}

	private Map<String, String> createRequestMapForSearch(String keyword,
			String associateId) {

		Map<String, String> params = new HashMap<String, String>();

		params.put("Service", "AWSECommerceService");
		params.put("Version", getVersionString());
		params.put("Operation", "ItemSearch");
		params.put("Keywords", keyword);
		params.put("SearchIndex", "All");
		params.put("ResponseGroup", "ItemAttributes,Images,Offers");
		params.put("AssociateTag", associateId);

		return params;

	}

}
