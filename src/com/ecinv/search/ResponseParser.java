package com.ecinv.search;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.content.Context;
import android.content.res.Resources;
import android.os.Environment;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TableLayout;

import com.ecinv.activity.TableLayoutActivity;
import com.ecinv.util.Listing;
import com.invbox.R;

public class ResponseParser {
	private final static String TAG = "inVBoxParser";
	private static SimpleDateFormat dateFormat;
	private Resources resources;
	private Context context;
	private String clientName;

	public ResponseParser() {

	}

	public ResponseParser(Context context, String client) {
		synchronized (this) {
			if (dateFormat == null) {
				dateFormat = new SimpleDateFormat(
						"[\"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'\"]");
			}

			this.resources = context.getResources();
			this.context = context;
			this.clientName = client;
		}
	}

	public synchronized ArrayList<Listing> parseListings(String response,
			String reqType, String clientId) throws Exception {
		ArrayList<Listing> listings = null;
		// Log.w("responsestring", response);
		if ("JSON".equalsIgnoreCase(reqType) || "".equalsIgnoreCase(reqType)
				|| reqType == null) {
			// parseForJSON(response, listings, clientId);
		} else {
			listings = parseForXML(response, clientId);
		}
		/*
		 * JSONObject rootObj = new JSONObject(response); JSONArray itemList =
		 * rootObj .getJSONArray( this.resources
		 * .getString(R.string.ebay_tag_findItemsByKeywordsResponse))
		 * .getJSONObject(0) .getJSONArray( this.resources
		 * .getString(R.string.ebay_tag_searchResult)) .getJSONObject(0)
		 * .getJSONArray(this.resources.getString(R.string.ebay_tag_item)); int
		 * itemCount = itemList.length(); for (int itemIndex = 0; itemIndex <
		 * itemCount; itemIndex++) { try { Listing listing =
		 * this.parseListingForJSON(itemList .getJSONObject(itemIndex));
		 * listing.setAuctionSource(this.resources
		 * .getString(R.string.ebay_source_name)); listings.add(listing); }
		 * catch (JSONException jx) { /* if something goes wrong log &&nbsp;move
		 * to the next item Log.e(TAG, "parseListings: jsonResponse=" +
		 * response, jx); } }
		 */
		return (listings);
	}

	private synchronized void parseForJSON(String jsonResponse,
			ArrayList<Listing> listings, String client) throws Exception {
		JSONObject rootObj = new JSONObject(jsonResponse);
		JSONArray jobj = rootObj.getJSONArray(this.resources
				.getString(R.string.ebay_tag_findItemsByProductResponse));
		if ("".equals(jobj.getJSONObject(0).getString("ack"))
				|| "[\"failure\"]".equalsIgnoreCase(jobj.getJSONObject(0)
						.getString("ack")))
			throw new Exception("Failed to fetch data.");

		if (("[\"success\"]".equalsIgnoreCase(jobj.getJSONObject(0).getString(
				"ack")))) {

			JSONArray itemList = rootObj
					.getJSONArray(
							this.resources
									.getString(R.string.ebay_tag_findItemsByProductResponse))
					.getJSONObject(0)
					.getJSONArray(
							this.resources
									.getString(R.string.ebay_tag_searchResult))
					.getJSONObject(0)
					.getJSONArray(
							this.resources.getString(R.string.ebay_tag_item));
			int itemCount = itemList.length();
			for (int itemIndex = 0; itemIndex < itemCount; itemIndex++) {
				try {
					Listing listing = this.parseListingForJSON(
							itemList.getJSONObject(itemIndex), client);
					listing.setAuctionSource(this.resources
							.getString(R.string.ebay_source_name));
					listings.add(listing);
				} catch (JSONException jx) {
					/* if something goes wrong log &&nbsp;move to the next item */
					Log.e(TAG, "parseListings: jsonResponse=" + jsonResponse,
							jx);
				}
			}
		}
	}

	private synchronized ArrayList<Listing> parseForXML(String xmlResponse,
			String client) throws Exception {
		// implementation for XML response
		Document doc = xmlFromString(xmlResponse);
		String namespace = (doc.getNamespaceURI() != null ? doc
				.getNamespaceURI() : "");

		ArrayList<Listing> listings = new ArrayList<Listing>();
		// NodeList nodeList = doc.getElementsByTagName("ack");
		NodeList nodeList = doc
				.getElementsByTagName(getValueForElementForClient(
						client + "_ack_tag").coerceToString().toString());
		String ackValue = "";
		if (nodeList == null || nodeList.getLength() <= 0) {
			throw new Exception(
					"Seems to a problem while contacting the service.");
		}

		Node node = nodeList.item(0);
		ackValue = getElementValue(node);
		String failureVal = getValueForElementForClient(
				client + "_failure_value").coerceToString().toString();
		String failureComp = getValueForElementForClient(
				client + "_failure_comparator").coerceToString().toString();

		if ("".equals(ackValue)
				|| !isResponseValid(failureComp, failureVal, ackValue)) {
			// failureVal.equalsIgnoreCase(ackValue) ) {
			throw new Exception("Failed to fetch data.");
		}
		// _success_value
		String successComp = getValueForElementForClient(
				client + "_success_comparator").coerceToString().toString();
		if (isSuccess(successComp, ackValue,
				getValueForElementForClient(client + "_success_value")
						.coerceToString().toString())) {
			// need to change this
			NodeList nodes = doc
					.getElementsByTagName(getValueForElementForClient(
							client + "_tag_item").coerceToString().toString());

			String sourceName = "";
			for (int i = 0; i < nodes.getLength(); i++) {
				// HashMap<string, string=""> map = new HashMap<string,
				// string="">();
				Element e = (Element) nodes.item(i);
				Listing listing = parseListingForXML(e, namespace, client);
				if (sourceName == null || "".equals(sourceName)) {

				}
				getValueForElementForClient(getValueForElementForClient(
						client + "_source_name").coerceToString().toString());
				// this.resources.getValue(this.resources.getIdentifier(client
				// + "_source_name", "string",
				// this.context.getPackageName()), outValue, resolveRefs);
				// listing.setAuctionSource(this.resources
				// .getString(R.string.ebay_source_name));
				listing.setAuctionSource(getValueForElementForClient(
						client + "_source_name").coerceToString().toString());
				listings.add(listing);
				// map.put("id", XMLfunctions.getValue(e, "id"));
				// map.put("name", "Naam:" + XMLfunctions.getValue(e, "name"));
				// map.put("Score", "Score: " + XMLfunctions.getValue(e,
				// "score"));
				// mylist.add(map);
			}
		}// end of success

		return listings;

	}

	private static boolean isSuccess(String comparator, String actualValue,
			String successVal) {

		long value = -1;

		if ("<".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				value = Long.parseLong(actualValue.trim());
				if (value < Long.parseLong(successVal.trim())) {
					return true;
				}
			}
			return false;
		} else if (">".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				value = Long.parseLong(actualValue);
				if (value > Long.parseLong(successVal)) {
					return true;
				}
			}
			return false;

		} else if ("=".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				value = Long.parseLong(actualValue);
				if (value == Long.parseLong(successVal)) {
					return true;
				}
			}
			return false;

		} else if ("<=".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				value = Long.parseLong(actualValue);
				if (value <= Long.parseLong(successVal)) {
					return true;
				}
			}
			return false;
		} else if (">=".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				value = Long.parseLong(actualValue);
				if (value >= Long.parseLong(successVal)) {
					return true;
				}
			}
			return false;
		} else if ("equals".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				// value = Long.parseLong(actualValue);
				if (actualValue.equalsIgnoreCase(successVal)) {
					return true;
				}
			}
			return false;

		} else {
			return false;
		}
	}

	private static boolean isResponseValid(String comparator,
			String failureVal, String actualValue) {

		long value = -1;

		if ("<".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				value = Long.parseLong(actualValue.trim());
				if (value > Long.parseLong(failureVal.trim())) {
					return true;
				}
			}
			return false;
		} else if (">".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				value = Long.parseLong(actualValue);
				if (value < Long.parseLong(failureVal)) {
					return true;
				}
			}
			return false;

		} else if ("=".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				value = Long.parseLong(actualValue);
				if (value == Long.parseLong(failureVal)) {
					return true;
				}
			}
			return false;

		} else if ("<=".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				value = Long.parseLong(actualValue);
				if (value >= Long.parseLong(failureVal)) {
					return true;
				}
			}
			return false;
		} else if (">=".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				value = Long.parseLong(actualValue);
				if (value <= Long.parseLong(failureVal)) {
					return true;
				}
			}
			return false;
		} else if ("equals".equals(comparator)) {
			if (actualValue != null && !"".equals(actualValue)) {
				// value = Long.parseLong(actualValue);
				if (!actualValue.equalsIgnoreCase(failureVal)) {
					return true;
				}
			}
			return false;

		} else {
			return false;
		}

	}

	/**
	 * Returns element value
	 * 
	 * @param elem
	 *            element (it is XML tag)
	 * @return Element value otherwise empty String
	 */
	public final static String getElementValue(Node elem) {
		Node kid = null;
		try {
			if (elem != null) {
				if (elem.hasChildNodes()) {
					for (kid = elem.getFirstChild(); kid != null; kid = kid
							.getNextSibling()) {
						if (kid.getNodeType() == Node.TEXT_NODE) {
							return kid.getNodeValue();
						}
					}
				}
			}
		} catch (Exception e) {
			Log.w("getElementvalue::", kid.getLocalName());
			Log.w("getElementvalueEx::", e.getMessage());
		}
		return "";
	}

	private void writeToFile(String xml) {
		try {
			File myFile = new File(Environment.getExternalStorageDirectory()
					+ File.separator + "test.txt");
			myFile.createNewFile();
			FileOutputStream fOut = new FileOutputStream(myFile);
			OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
			myOutWriter.write(xml);
			myOutWriter.close();
			fOut.close();
		} catch (Exception e) {
			Log.w("xmlexception", e);
		}
	}

	public synchronized Document xmlFromString(String xml) {

		Document doc = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Log.w("xmlresponse", xml);
		try {
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = null;

			if ("ebay".equalsIgnoreCase(this.clientName)) {
				transformer = tFactory.newTransformer(new StreamSource(
						this.context.getResources().openRawResource(
								R.raw.sortebay)));
				Writer outWriter = new StringWriter();
				StreamResult result = (StreamResult) new StreamResult(outWriter);
				transformer.transform(new StreamSource(
						new ByteArrayInputStream(xml.getBytes())), result);
				xml = ((StringWriter) result.getWriter()).getBuffer()
						.toString();

			} else if ("google".equalsIgnoreCase(this.clientName)) {
				transformer = tFactory.newTransformer(new StreamSource(
						this.context.getResources().openRawResource(
								R.raw.sortgoogle)));
				Writer outWriter = new StringWriter();
				StreamResult result = (StreamResult) new StreamResult(outWriter);
				transformer.transform(new StreamSource(
						new ByteArrayInputStream(xml.getBytes())), result);
				xml = ((StringWriter) result.getWriter()).getBuffer()
						.toString();
//				writeToFile(xml);
			} else if ("amazon".equalsIgnoreCase(this.clientName)) {
				transformer = tFactory.newTransformer(new StreamSource(
						this.context.getResources().openRawResource(
								R.raw.sortamazon)));
				Writer outWriter = new StringWriter();
				StreamResult result = (StreamResult) new StreamResult(outWriter);
				transformer.transform(new StreamSource(
						new ByteArrayInputStream(xml.getBytes())), result);
				xml = ((StringWriter) result.getWriter()).getBuffer()
						.toString();
//				writeToFile(xml);
			} else if ("invbox".equalsIgnoreCase(this.clientName)) {
				transformer = tFactory.newTransformer(new StreamSource(
						this.context.getResources().openRawResource(
								R.raw.sortinvbox)));
			}

			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));
			doc = db.parse(is);

		} catch (ParserConfigurationException e) {
			Log.w("ParserConfigurationException", e);
			return null;
		} catch (SAXException e) {
			Log.w("SAXException", e);
			return null;
		} catch (IOException e) {
			Log.w("IOException", e);
			return null;
		}
		/*
		 * catch (TransformerConfigurationException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); } catch (TransformerException e) {
		 * // TODO Auto-generated catch block e.printStackTrace(); }
		 */catch (Exception e) {
			Log.e("transforming", e.getMessage());
		}

		return doc;
	}

	private Listing parseListingForJSON(JSONObject jsonObj, String client)
			throws JSONException {
		/*
		 * Things outside of a try/catch block are fields that are required and
		 * should throw an exception if not found Things inside of a try/catch
		 * block are fields we can live without
		 */
		Listing listing = new Listing();
		/*
		 * get items at the root of the object id, title, and URL are required
		 * image and location are optional
		 */
		TypedValue outValue = new TypedValue();
		boolean resolveRefs = false;

		// listing.setAuctionSource(this.resources
		// .getString(R.string.ebay_source_name));
		listing.setAuctionSource(getValueForElementForClient(
				client + "_source_name").coerceToString().toString());

		// listing.setId(jsonObj.getString(this.resources
		// .getString(R.string.ebay_tag_itemId)));

		listing.setId(jsonObj.getString(getValueForElementForClient(
				client + "_tag_itemId").coerceToString().toString()));

		// listing.setTitle(this.stripWrapper(jsonObj.getString(this.resources
		// .getString(R.string.ebay_tag_title))));

		listing.setTitle(this.stripWrapper(jsonObj
				.getString(getValueForElementForClient(client + "_tag_title")
						.coerceToString().toString())));

		// listing.setListingUrl(this.stripWrapper(jsonObj
		// .getString(this.resources
		// .getString(R.string.ebay_tag_viewItemURL))));

		listing.setListingUrl(this.stripWrapper(jsonObj
				.getString(getValueForElementForClient(
						client + "_tag_viewItemURL").coerceToString()
						.toString())));

		try {
			// listing.setImageUrl(this.stripWrapper(jsonObj
			// .getString(this.resources
			// .getString(R.string.ebay_tag_galleryURL))));
			listing.setImageUrl(this.stripWrapper(jsonObj
					.getString(getValueForElementForClient(
							client + "_tag_galleryURL").coerceToString()
							.toString())));
		} catch (JSONException jx) {
			Log.e(TAG, "parseListing: parsing image URL", jx);
			listing.setImageUrl(null);
		}
		try {
			// listing.setLocation(this.stripWrapper(jsonObj
			// .getString(this.resources
			// .getString(R.string.ebay_tag_location))));
			listing.setLocation(this.stripWrapper(jsonObj
					.getString(getValueForElementForClient(
							client + "_tag_location").coerceToString()
							.toString())));
		} catch (JSONException jx) {
			Log.e(TAG, "parseListing: parsing location", jx);
			listing.setLocation(null);
		}
		// get stuff under sellingStatus - required
		// JSONObject sellingStatusObj = jsonObj.getJSONArray(
		// this.resources.getString(R.string.ebay_tag_sellingStatus))
		// .getJSONObject(0);

		JSONObject sellingStatusObj = jsonObj.getJSONArray(
				getValueForElementForClient(client + "_tag_sellingStatus")
						.coerceToString().toString()).getJSONObject(0);

		// JSONObject currentPriceObj = sellingStatusObj.getJSONArray(
		// this.resources.getString(R.string.ebay_tag_currentPrice))
		// .getJSONObject(0);
		JSONObject currentPriceObj = sellingStatusObj.getJSONArray(
				getValueForElementForClient(client + "_tag_currentPrice")
						.coerceToString().toString()).getJSONObject(0);

		// listing.setCurrentPrice(this.formatCurrency(currentPriceObj
		// .getString(this.resources.getString(R.string.ebay_tag_value)),
		// currentPriceObj.getString(this.resources
		// .getString(R.string.ebay_tag_currencyId))));

		listing.setCurrentPrice(this.formatCurrency(currentPriceObj
				.getString(outValue.coerceToString().toString()),
				currentPriceObj
						.getString(getValueForElementForClient(
								client + "_tag_currencyId").coerceToString()
								.toString())));

		// get stuff under shippingInfo - optional
		try {
			// JSONObject shippingInfoObj = jsonObj.getJSONArray(
			// this.resources.getString(R.string.ebay_tag_shippingInfo))
			// .getJSONObject(0);

			JSONObject shippingInfoObj = jsonObj.getJSONArray(
					getValueForElementForClient(client + "_tag_shippingInfo")
							.coerceToString().toString()).getJSONObject(0);

			// JSONObject shippingServiceCostObj = shippingInfoObj.getJSONArray(
			// this.resources
			// .getString(R.string.ebay_tag_shippingServiceCost))
			// .getJSONObject(0);

			JSONObject shippingServiceCostObj = shippingInfoObj.getJSONArray(
					getValueForElementForClient(
							client + "_tag_shippingServiceCost")
							.coerceToString().toString()).getJSONObject(0);

			// listing.setShippingCost(this.formatCurrency(shippingServiceCostObj
			// .getString(this.resources
			// .getString(R.string.ebay_tag_value)),
			// currentPriceObj.getString(this.resources
			// .getString(R.string.ebay_tag_currencyId))));
			this.resources.getValue(this.resources.getIdentifier(client
					+ "_tag_value", "string", this.context.getPackageName()),
					outValue, resolveRefs);

			listing.setShippingCost(this.formatCurrency(shippingServiceCostObj
					.getString(getValueForElementForClient(
							client + "_tag_currencyId").coerceToString()
							.toString()), currentPriceObj
					.getString(getValueForElementForClient(
							client + "_tag_currencyId").coerceToString()
							.toString())));

		} catch (JSONException jx) {
			Log.e(TAG, "parseListing: parsing shipping cost", jx);
			listing.setShippingCost("Not listed");
		}
		// get stuff under listingInfo
		try {
			// JSONObject listingInfoObj = jsonObj.getJSONArray(
			// this.resources.getString(R.string.ebay_tag_listingInfo))
			// .getJSONObject(0);
			JSONObject listingInfoObj = jsonObj.getJSONArray(
					getValueForElementForClient(client + "_tag_listingInfo")
							.coerceToString().toString()).getJSONObject(0);
			try {
				// String listingType = this.stripWrapper(listingInfoObj
				// .getString(this.resources
				// .getString(R.string.ebay_tag_listingType)));
				String listingType = this.stripWrapper(listingInfoObj
						.getString(getValueForElementForClient(
								client + "_tag_listingType").coerceToString()
								.toString()));

				// if (listingType.toLowerCase().indexOf(
				// this.resources.getString(R.string.ebay_value_auction)) > -1)
				// {
				if (listingType.toLowerCase().indexOf(
						getValueForElementForClient(client + "_value_auction")
								.coerceToString().toString()) > -1) {
					listing.setAuction(true);
					try {
						// String buyItNowAvailable = this
						// .stripWrapper(listingInfoObj.getString(this.resources
						// .getString(R.string.ebay_tag_buyItNowAvailable)));
						String buyItNowAvailable = this
								.stripWrapper(listingInfoObj
										.getString(getValueForElementForClient(
												client
														+ "_tag_buyItNowAvailable")
												.coerceToString().toString()));

						if (buyItNowAvailable
								.equalsIgnoreCase(getValueForElementForClient(
										client + "_value_true")
										.coerceToString().toString())) {
							listing.setBuyItNow(true);
						} else {
							listing.setBuyItNow(false);
						}
					} catch (JSONException jx) {
						Log.e(TAG, "parseListing: parsing but it now", jx);
					}
				} else {
					listing.setAuction(false);
					listing.setBuyItNow(true);
				}
			} catch (JSONException jx) {
				Log.e(TAG, "parseListing: parsing listing type", jx);
			}
			// get start and end dates - optional
			try {
				// Date startTime = dateFormat.parse(listingInfoObj
				// .getString(this.resources
				// .getString(R.string.ebay_tag_startTime)));
				Date startTime = dateFormat.parse(listingInfoObj
						.getString(getValueForElementForClient(
								client + "_tag_startTime").coerceToString()
								.toString()));
				listing.setStartTime(startTime);
				// Date endTime = dateFormat.parse(listingInfoObj
				// .getString(this.resources
				// .getString(R.string.ebay_tag_endTime)));

				Date endTime = dateFormat.parse(listingInfoObj
						.getString(getValueForElementForClient(
								client + "_tag_endTime").coerceToString()
								.toString()));
				listing.setEndTime(endTime);
			} catch (Exception x) { // generic - both ParseException and
									// JSONException can be thrown, same result
									// either way
				Log.e(TAG, "parseListing: parsing start and end dates", x);
				listing.setStartTime(null);
				listing.setEndTime(null);
			}
		} catch (JSONException jx) {
			Log.e(TAG, "parseListing: parsing listing info", jx);
			listing.setStartTime(null);
			listing.setEndTime(null);
		}
		// alright, all done
		return (listing);
	}

	private synchronized Listing parseListingForXML(Element element, String ns,
			String client) {
		/*
		 * Things outside of a try/catch block are fields that are required and
		 * should throw an exception if not found Things inside of a try/catch
		 * block are fields we can live without
		 */
		Listing listing = new Listing();
		/*
		 * get items at the root of the object id, title, and URL are required
		 * image and location are optional
		 */
		try {
			listing.setSiteName(client);
			NodeList tempNL = element
					.getElementsByTagName(getValueForElementForClient(
							client + "_tag_itemId").coerceToString().toString());
			if (tempNL != null && tempNL.getLength() > 0)
				listing.setId(getElementValue((Node) tempNL.item(0)));

			tempNL = element.getElementsByTagName(getValueForElementForClient(
					client + "_tag_title").coerceToString().toString());

			if (tempNL != null && tempNL.getLength() > 0)
				listing.setTitle(getElementValue((Node) tempNL.item(0)));

			tempNL = element.getElementsByTagName(getValueForElementForClient(
					client + "_tag_viewItemURL").coerceToString().toString());
			if (tempNL != null && tempNL.getLength() > 0) {
				listing.setListingUrl(getElementValue((Node) tempNL.item(0)));
			}

//			 tempNL =
//			 element.getElementsByTagName(getValueForElementForClient(
//			 client + "_tag_galleryURL").coerceToString().toString());
			
			tempNL = evaluateXpathAndReturnNodes(element,
					getValueForElementForClient(client + "_tag_galleryURL")
							.coerceToString().toString());

			if (tempNL != null && tempNL.getLength() > 0) {
				String imageVal = checkAndGetValueForElement(client
						+ "_tag_galleryURL", (Element) tempNL.item(0));
				if (imageVal != null && !"".equals(imageVal)) {
					listing.setImageUrl(imageVal);
				} else {
					listing.setImageUrl(getElementValue((Node) tempNL.item(0)));
				}
				// Log.w("imageurl", getElementValue((Node) tempNL.item(0)));
			}

			tempNL = element.getElementsByTagName(getValueForElementForClient(
					client + "_tag_location").coerceToString().toString());

			if (tempNL != null && tempNL.getLength() > 0)
				listing.setLocation(getElementValue((Node) tempNL.item(0)));

			// get stuff under sellingStatus - required
			tempNL = element.getElementsByTagName(getValueForElementForClient(
					client + "_tag_sellingStatus").coerceToString().toString());

			Element sellingStatusObj = null;
			if (tempNL != null && tempNL.getLength() > 0)
				sellingStatusObj = (Element) tempNL.item(0);

			// the if is written for invbox as intially didn't hv the selling
			// info
			if (sellingStatusObj != null) {
				tempNL = sellingStatusObj
						.getElementsByTagName(getValueForElementForClient(
								client + "_tag_currentPrice").coerceToString()
								.toString());
			}

			Element currentPriceObj = null;
			if (tempNL != null && tempNL.getLength() > 0)
				currentPriceObj = (Element) tempNL.item(0);

			// the if is written for invbox as intially didn't hv the current
			// price info
			if (currentPriceObj != null) {
				// dynamically getting data
				listing.setCurrentPrice(this.formatCurrency(
						getElementValue(currentPriceObj),
						checkAndGetValueForElement(client + "_tag_currencyId",
								currentPriceObj)));

				listing.setCurrentPrice(this.formatCurrency(
						getElementValue(currentPriceObj), currentPriceObj
								.getAttribute(getValueForElementForClient(
										client + "_tag_currencyId")
										.coerceToString().toString())));
			}
			// get stuff under shippingInfo - optional
			tempNL = element.getElementsByTagName(getValueForElementForClient(
					client + "_tag_shippingInfo").coerceToString().toString());

			Element shippingInfoElem = null;
			if (tempNL != null && tempNL.getLength() > 0)
				shippingInfoElem = (Element) tempNL.item(0);

			if (shippingInfoElem != null) {
				tempNL = shippingInfoElem
						.getElementsByTagName(getValueForElementForClient(
								client + "_tag_shippingServiceCost")
								.coerceToString().toString());
			}

			Element shippingServiceCostElem = null;
			if (tempNL != null && tempNL.getLength() > 0)
				shippingServiceCostElem = (Element) tempNL.item(0);

			if (shippingServiceCostElem != null) {
				listing.setShippingCost(this.formatCurrency(
						getElementValue(shippingServiceCostElem),
						currentPriceObj
								.getAttribute(getValueForElementForClient(
										client + "_tag_currencyId")
										.coerceToString().toString())));
			}
			// get stuff under listingInfo

			tempNL = element.getElementsByTagName(getValueForElementForClient(
					client + "_tag_listingInfo").coerceToString().toString());

			Element listingInfoElement = null;
			if (tempNL != null && tempNL.getLength() > 0)
				listingInfoElement = (Element) tempNL.item(0);

			if (listingInfoElement != null) {
				tempNL = listingInfoElement
						.getElementsByTagName(getValueForElementForClient(
								client + "_tag_listingType").coerceToString()
								.toString());
			}
			String listingType = "";
			if (tempNL != null && tempNL.getLength() > 0)
				listingType = this.stripWrapper(getElementValue((Node) tempNL
						.item(0)));

			if (listingType != null
					&& listingType.toLowerCase().indexOf(
							getValueForElementForClient(
									client + "_value_auction").coerceToString()
									.toString()) > -1) {
				listing.setAuction(true);

				if (listingInfoElement != null) {
					tempNL = listingInfoElement
							.getElementsByTagName(getValueForElementForClient(
									client + "_tag_buyItNowAvailable")
									.coerceToString().toString());
				}
				String buyItNowAvailable = "";
				if (tempNL != null && tempNL.getLength() > 0)
					listingType = this
							.stripWrapper(getElementValue((Node) tempNL.item(0)));

				if (buyItNowAvailable
						.equalsIgnoreCase(getValueForElementForClient(
								client + "_value_true").coerceToString()
								.toString())) {
					listing.setBuyItNow(true);
				} else {
					listing.setBuyItNow(false);
				}

			} else {
				listing.setAuction(false);
				listing.setBuyItNow(true);
			}

			// get start and end dates - optional
			// try {
			// Date startTime = dateFormat.parse(listingInfoObj
			// .getString(this.resources
			// .getString(R.string.ebay_tag_startTime)));

			// tempNL = listingInfoElement.getElementsByTagName(this.resources
			// .getString(R.string.ebay_tag_startTime));
			// Date startTime = null;
			// if (tempNL != null && tempNL.getLength() > 0)
			// startTime = dateFormat.parse(getElementValue((Node) tempNL
			// .item(0)));
			//
			// Log.w("sdate",
			// ""
			// + dateFormat.parse(getElementValue((Node) tempNL
			// .item(0))));
			// listing.setStartTime(startTime);

			// Date endTime = dateFormat.parse(listingInfoObj //
			// .getString(this.resources //
			// .getString(R.string.ebay_tag_endTime)));

			// tempNL = listingInfoElement.getElementsByTagName(this.resources
			// .getString(R.string.ebay_tag_endTime));
			//
			// Date endTime = null;
			// if (tempNL != null && tempNL.getLength() > 0)
			// endTime = dateFormat.parse(getElementValue((Node) tempNL
			// .item(0)));
			// listing.setStartTime(startTime);
			// listing.setEndTime(endTime);

		} catch (Exception x) { // generic - both ParseException and
								// JSONException can be thrown, same result
								// either way
								// Log.e(TAG,
								// "parseListing: parsing start and end dates",
								// x);
			Log.e("sdate", "" + x.getMessage());
			listing.setStartTime(null);
			listing.setEndTime(null);
		}

		// alright, all done
		return (listing);
	}

	private String getAttributeValue(String elementName) {
		TypedValue element = getValueForElementForClient(elementName + "_attr");
		String value = "";
		if (element != null) {
			value = element.coerceToString().toString();
		}
		return value;
	}

	private NodeList evaluateXpathAndReturnNodes(Element element,
			String elementName) {

		return getExactNode(element, elementName);

	}

	private String checkAndGetValueForElement(String elementName,
			Element element) {

		Element tempElem = null;
		String value = "";
		String attribVal = getAttributeValue(elementName);
		if (attribVal != null && !"".equals(attribVal)) {
			// use getattribute
			value = element.getAttribute(attribVal);
		} else {
			// use elementbytagname
			NodeList tempNL = element
					.getElementsByTagName(getValueForElementForClient(
							elementName).coerceToString().toString());

			if (tempNL != null && tempNL.getLength() > 0) {
				tempElem = (Element) tempNL.item(0);
				if (tempElem != null) {
					value = getElementValue((Node) tempElem);
				}
			}
		}
		return value;
	}

	private NodeList getExactNode(Element element, String tagName) {

		String[] tagnames = tagName.split("/");
		Element ele = element;
		int count = 0;
		for (; count < tagnames.length - 1; count++) {
			NodeList nodeList = ele.getElementsByTagName(tagnames[count]);
			Element tempElem = null;

			if (nodeList != null && nodeList.getLength() > 0) {
				tempElem = (Element) nodeList.item(0);
				if (tempElem != null) {
					// value = getElementValue((Node) tempElem);
					ele = tempElem;
				}
			}
		}

		return ele.getElementsByTagName(tagnames[count]);

	}

	private TypedValue getValueForElementForClient(String elementName) {
		TypedValue outValue = null;
		boolean resolveRefs = false;
		int id = this.resources.getIdentifier(elementName, "string",
				this.context.getPackageName());

		if (id > 0) {
			outValue = new TypedValue();
			this.resources.getValue(id, outValue, resolveRefs);
		}
		return outValue;
	}

	private String formatCurrency(String amount, String currencyCode) {
		StringBuffer formattedText = new StringBuffer(amount);
		try {
			// add trailing zeros
			int indexOf = formattedText.indexOf(".");
			if (indexOf >= 0) {
				if (formattedText.length() - indexOf == 2) {
					formattedText.append("0");
				}
			}
			// add dollar sign
			if (currencyCode.equalsIgnoreCase("USD")) {
				formattedText.insert(0, "$");
			} else {
				formattedText.append(" ");
				formattedText.append(currencyCode);
			}
		} catch (Exception x) {
			Log.e(TAG, "formatCurrency", x);
		}
		return (formattedText.toString());
	}

	private String stripWrapper(String s) {
		try {
			int end = s.length() - 2;
			return (s.substring(2, end));
		} catch (Exception x) {
			Log.e(TAG, "stripWrapper", x);
			return (s);
		}
	}
}

/*
 * public String calcHmac(String src) throws Exception {
 * 
 * String key = "d6fc3a4a06ed55d24fecde188aaa9161"; Mac mac =
 * Mac.getInstance("HmacSHA1"); SecretKeySpec sk = new
 * SecretKeySpec(key.getBytes(),mac.getAlgorithm()); mac.init(sk); byte[] result
 * = mac.doFinal(src.getBytes());
 * 
 * 
 * return Base64.encodeToString(result ,Base64.URL_SAFE); } static byte[]
 * HmacSHA256(String data, byte[] key) throws Exception { String
 * algorithm="HmacSHA256"; Mac mac = Mac.getInstance(algorithm); mac.init(new
 * SecretKeySpec(key, algorithm)); return mac.doFinal(data.getBytes("UTF8")); }
 * 
 * static byte[] getSignatureKey(String key, String dateStamp, String
 * regionName, String serviceName) throws Exception { byte[] kSecret = ("AWS4" +
 * key).getBytes("UTF8"); byte[] kDate = HmacSHA256(dateStamp, kSecret); byte[]
 * kRegion = HmacSHA256(regionName, kDate); byte[] kService =
 * HmacSHA256(serviceName, kRegion); byte[] kSigning =
 * HmacSHA256("aws4_request", kService); return kSigning; }
 */