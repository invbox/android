package com.ecinv.search;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;

import com.ecinv.search.helper.RequestHelper;

public class RequestInvoker {
	private static String appID;
	private static String ebayURL;
	private Resources resources;
	private Context context;
	private String client;

	public RequestInvoker(Context context, String client) {
		this.client = client;
		this.context = context;
		this.resources = context.getResources();
		System.setProperty("http.proxyHost", "webproxy.redbeemedia.net");
		System.setProperty("http.proxyPort", "8080");
		/*
		 * if (Build.PRODUCT.toLowerCase().indexOf("sdk") > -1) {
		 * 
		 * the sandbox URLs are pretty useless as they only return a success
		 * code but no results if you really want to use them then swap out the
		 * next two lines
		 * appID=this.resources.getString(R.string.ebay_appid_sandbox);
		 * ebayURL=this.resources.getString(R.string.ebay_wsurl_sandbox);
		 * 
		 * appID = getValueForElementForClient( this.client +
		 * "_appid_production").coerceToString() .toString(); ebayURL =
		 * getValueForElementForClient( this.client +
		 * "_wsurl_production").coerceToString() .toString(); } else { appID =
		 * getValueForElementForClient( this.client +
		 * "_appid_production").coerceToString() .toString(); ebayURL =
		 * getValueForElementForClient( this.client +
		 * "_wsurl_production").coerceToString() .toString(); }
		 */
	}

/*	public synchronized String search(String keyword, String type,
			String ebaySite, String reqType, String byType, String client)
			throws Exception {
		String response = null;
		appID = getValueForElementForClient(client + "_appid_production")
				.coerceToString().toString();
		ebayURL = getValueForElementForClient(client + "_wsurl_production")
				.coerceToString().toString();
		try {
			String templateType = "";
			if ("keyword".equalsIgnoreCase(byType)) {
				templateType = getValueForElementForClient(
						client + "_request_template_by_keyword")
						.coerceToString().toString();
			} else {
				templateType = getValueForElementForClient(
						client + "_request_template_by_product")
						.coerceToString().toString();
			}
			response = invokeEbayRest(keyword, type, ebaySite, reqType,
					templateType, ebayURL, appID);
			if ((response == null) || (response.length() < 1)) {
				throw (new Exception("No result received from invokeEbayRest("
						+ keyword + ")"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.w("ebayException", e.getMessage());
		}
		return (response);
	}*/

	private synchronized TypedValue getValueForElementForClient(
			String elementName) {
		TypedValue outValue = new TypedValue();
		boolean resolveRefs = false;
		
		int identifier = this.resources.getIdentifier(elementName, "string",	this.context.getPackageName());

		if( identifier != 0){
		this.resources
				.getValue( identifier, outValue, resolveRefs);
		}
		return outValue;
	}

	public synchronized String search(String keyword, String ebaySite,
			String reqType, String byType, String client) throws Exception {
		String response = null;
		appID = getValueForElementForClient(client + "_appid_production")
				.coerceToString().toString();
		ebayURL = getValueForElementForClient(client + "_wsurl_production")
				.coerceToString().toString();
		try {
			String templateType = "";
			if ("keyword".equalsIgnoreCase(byType)) {
				templateType = getValueForElementForClient(
						client + "_request_template_by_keyword")
						.coerceToString().toString();
			} else {
				templateType = getValueForElementForClient(
						client + "_request_template_by_product")
						.coerceToString().toString();
			}
			response = invokeRestApi(keyword, ebaySite, reqType, templateType,
					ebayURL, appID, client);
			if ((response == null) || (response.length() < 1)) {
				throw (new Exception("No result received from invokeEbayRest("
						+ keyword + ")"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.w("ebayException", e.getMessage());
		}
		return (response);
	}

/*	private synchronized String getRequestURL(String keyword, String type,
			String ebaySite, String reqType, String templateId, String url,
			String appID) {
		CharSequence requestURL = TextUtils.expandTemplate(templateId, ebayURL,
				appID, reqType, type, keyword, ebaySite);
		// Log.w("ramprasad",requestURL.toString());
		return (requestURL.toString());
	}*/

	/*private synchronized String invokeEbayRest(String keyword, String type,
			String ebaySite, String reqType, String templateId, String url,
			String appID) throws Exception {
		String result = null;
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(this.getRequestURL(keyword, type,
				ebaySite, reqType, templateId, url, appID));
		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity httpEntity = response.getEntity();
		if (httpEntity != null) {
			InputStream in = httpEntity.getContent();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuffer temp = new StringBuffer();
			String currentLine = null;
			while ((currentLine = reader.readLine()) != null) {
				temp.append(currentLine);
			}
			result = temp.toString();
			in.close();
		}
		return (result);
	}*/

//	private synchronized String getRequestURL(String keyword, String ebaySite,
//			String reqType, String templateId, String url, String appID) {
//		CharSequence requestURL = TextUtils.expandTemplate(templateId, ebayURL,
//				appID, reqType, keyword, ebaySite);
//		// Log.w("ramprasad",requestURL.toString());
//		return (requestURL.toString());
//	}
	
	private synchronized String getRequestURL(String keyword, String ebaySite,
			String reqType, String templateId, String url, String appID, String client) {
		TypedValue val =  getValueForElementForClient(client + "_request_helper_class");
		String helperClassname = "";
		String requestURLStr = "";
		
		if( val != null && val.coerceToString() != null){
			helperClassname = val.coerceToString().toString();
		}
		if (helperClassname != null && !"".equals(helperClassname)) {
			RequestHelper rh;
			try {
				rh = (RequestHelper) Class.forName(helperClassname).newInstance();
				rh.setResources(this.resources);
				rh.setContext(this.context);
				requestURLStr = rh.prepareRequestString(keyword, ebaySite, reqType, templateId,
						url, appID,client);
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			CharSequence requestURL = TextUtils.expandTemplate(templateId,
					ebayURL, appID, reqType, keyword, ebaySite);
			// Log.w("ramprasad",requestURL.toString());
			requestURLStr = requestURL.toString();
		}
		return (requestURLStr);
	}

	private synchronized String invokeRestApi(String keyword, String ebaySite,
			String reqType, String templateId, String url, String appID, String client)
			throws Exception {
		String result = null;
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(this.getRequestURL(keyword, ebaySite,
				reqType, templateId, url, appID, client));
		HttpResponse response = httpClient.execute(httpGet);
		HttpEntity httpEntity = response.getEntity();
		if (httpEntity != null) {
			InputStream in = httpEntity.getContent();
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(in));
			StringBuffer temp = new StringBuffer();
			String currentLine = null;
			while ((currentLine = reader.readLine()) != null) {
				temp.append(currentLine);
			}
			result = temp.toString();
			in.close();
		}
		return (result);
	}
}