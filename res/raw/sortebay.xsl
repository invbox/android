
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ebay="http://www.ebay.com/marketplace/search/v1/services" version="1.0">
	<xsl:output indent="yes" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ebay:searchResult">
		<xsl:copy>
			<xsl:apply-templates select="@* | *">
				<xsl:sort data-type="text" select="ebay:sellingStatus/ebay:currentPrice"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>