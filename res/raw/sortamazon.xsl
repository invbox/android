
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:amazon="http://webservices.amazon.com/AWSECommerceService/2011-08-01" version="1.0">
	<xsl:output indent="yes" omit-xml-declaration="yes"/>
	<xsl:strip-space elements="*"/>
	<xsl:template match="@* | node()">
		<xsl:copy>
			<xsl:apply-templates select="@* | node()" />
		</xsl:copy>
	</xsl:template>
	<xsl:template match="amazon:Items">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*">
				<xsl:sort select="amazon:OfferSummary/amazon:LowestNewPrice/amazon:FormattedPrice" order="ascending"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>