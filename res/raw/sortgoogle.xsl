<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:gd="http://schemas.google.com/g/2005"
    xmlns:google="http://www.w3.org/2005/Atom"
    xmlns:openSearch="http://a9.com/-/spec/opensearchrss/1.0/"
    xmlns:s="http://www.google.com/shopping/api/schemas/2010"
    version="1.0" >

    <xsl:output
        indent="yes"
        omit-xml-declaration="yes" />

    <xsl:strip-space elements="*" />

    <xsl:template match="@* | node()" >

        <xsl:copy >

            <xsl:apply-templates select="@* | node()" />
        </xsl:copy>
    </xsl:template>

    <xsl:template match="google:feed" >

        <xsl:copy >

            <xsl:apply-templates select="@* | *" >

                <xsl:sort
                    data-type="number"
                    select="s:product/s:inventories/s:inventory/s:price" />
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>

    <!--
    another   Working  one                              
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* 
                | node()[not(self::google:entry[parent::google:feed])]"/>
            <xsl:apply-templates select="google:entry[parent::google:feed]">
                <xsl:sort data-type="number" select="s:product/s:inventories/s:inventory/s:price"/>
            </xsl:apply-templates>
        </xsl:copy>
    </xsl:template>
    -->

</xsl:stylesheet>